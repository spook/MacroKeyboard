﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using MacroKeyboard.Shared.Resources;
using System.Globalization;
using MacroKeyboard.Shared.Extensions;

namespace MacroKeyboard.Shared.Converters
{
    public class EnumResourceConverter : EnumConverter
    {
        private ResourceManager resourceManager;

        public EnumResourceConverter(Type type)
            : base(type)
        {
            resourceManager = new ResourceManager(typeof(Strings));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (value != null)
                    return value.GetLocalizedName();
                else
                    return null;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
