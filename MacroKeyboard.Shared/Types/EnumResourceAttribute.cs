﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Types
{
    public class EnumResourceAttribute : Attribute
    {
        public EnumResourceAttribute(string key)
        {
            Key = key;
        }

        public string Key { get; }       
    }
}
