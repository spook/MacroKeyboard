﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Types
{
    public enum Actions : byte
    {
        KeyPress = 0,
        ModifierDown = 1,
        ModifierUp = 2,
        SwitchToScreen = 3,
        ToggleEncoderMode = 4,
        Sleep = 5,
        SwitchToEncoderMode = 6,
        Execute = 7,
        Activate = 8
    }
}
