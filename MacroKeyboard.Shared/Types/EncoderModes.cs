﻿using MacroKeyboard.Shared.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Types
{
    [TypeConverter(typeof(EnumResourceConverter))]
    public enum EncoderModes : byte
    {
        [EnumResource("EncoderMode_Arrows")]Arrows = 0,
        [EnumResource("EncoderMode_Mouse")]Mouse = 1,
        [EnumResource("EncoderMode_Scroll")]Scroll = 2,
        [EnumResource("EncoderMode_Volume")]Volume = 3,
        [EnumResource("EncoderMode_CtrlArrows")]CtrlArrows = 4
    }
}
