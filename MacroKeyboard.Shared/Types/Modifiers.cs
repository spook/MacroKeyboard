﻿using MacroKeyboard.Shared.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Types
{
    [TypeConverter(typeof(EnumResourceConverter))]
    public enum Modifiers : ushort
    {
        [EnumResource("Key_LeftCtrl")] [XmlEnum("LeftCtrl")] LeftCtrl = 0x01 | 0xe000,
        [EnumResource("Key_LeftShift")] [XmlEnum("LeftShift")] LeftShift = 0x02 | 0xe000,
        [EnumResource("Key_LeftAlt")] [XmlEnum("LeftAlt")] LeftAlt = 0x04 | 0xe000,
        [EnumResource("Key_LeftWindows")] [XmlEnum("LeftWindows")] LeftWindows = 0x08 | 0xe000,
        [EnumResource("Key_RightCtrl")] [XmlEnum("RightCtrl")] RightCtrl = 0x10 | 0xe000,
        [EnumResource("Key_RightShift")] [XmlEnum("RightShift")] RightShift = 0x20 | 0xe000,
        [EnumResource("Key_RightAlt")] [XmlEnum("RightAlt")] RightAlt = 0x40 | 0xe000,
        [EnumResource("Key_RightWindows")] [XmlEnum("RightWindows")] RightWindows = 0x80 | 0xe000
,    }
}
