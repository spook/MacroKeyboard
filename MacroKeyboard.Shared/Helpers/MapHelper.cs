﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Helpers
{
    public static class MapHelper
    {
        private static int sections = -1;

        public static void AddHeader(StreamWriter sw)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine()
                .Append("Offset".PadRight(7))
                .Append(" | ")
                .Append("Bytes".PadRight(16))
                .Append(" | ")
                .Append("Value".PadRight(16))
                .Append(" | ")
                .AppendLine("Description")
                .Append(string.Empty.PadLeft(7, '-'))
                .Append("-+-")
                .Append(string.Empty.PadRight(16, '-'))
                .Append("-+-")
                .Append(string.Empty.PadRight(16, '-'))
                .Append("-+-")
                .Append(string.Empty.PadRight(32, '-'));

            sw.WriteLine(sb.ToString());
        }

        public static void AddSection(StreamWriter writer, string section)
        {
            sections++;
            if (sections % 10 == 0)
            {
                AddHeader(writer);
                sections = 0;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Empty.PadLeft(7))
                .Append(" | ")
                .Append(string.Empty.PadRight(16))
                .Append(" | ")
                .Append(string.Empty.PadRight(16))
                .Append(" | ")
                .Append(section);
            writer.WriteLine(sb.ToString());
        }

        public static void AddEntry(StreamWriter writer, int offset, string value, string byteCount, string description)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(offset.ToString().PadLeft(7))
                .Append(" | ")
                .Append(byteCount.PadRight(16))
                .Append(" | ")
                .Append(value.PadRight(16))
                .Append(" | ")
                .Append(description);

            writer.WriteLine(sb.ToString());
        }
    }
}
