﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models.Validation
{
    public class ValidationError
    {
        private string GetMessageRecursive(string indent)
        {
            string innerMessage = Inner?.GetMessageRecursive($"  {indent}") ?? string.Empty;
            if (innerMessage != null)
            {
                return $"{indent}{Path}: {Message}\r\n{innerMessage}";
            }
            else
                return $"{indent}{Path}: {Message}";
        }

        public ValidationError(string path, string message, ValidationError? inner = null)
        {
            Path = path;
            Message = message;
            Inner = inner;
        }

        public override string ToString()
        {
            return GetMessageRecursive("");
        }

        public string Path { get; }
        public string Message { get; }
        public ValidationError? Inner { get; }
    }
}
