﻿using MacroKeyboard.Shared.Models.Validation;
using MacroKeyboard.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class KeyPressAction : BaseAction
    {
        public override ValidationError? Validate(string definitionPath)
        {
            if (Key == 0)
                return new ValidationError("KeyPressAction", $"Key {Key} does not match any know key name!");

            return null;
        }

        protected override string GetHeader() => "--- Key press action ---";

        protected override (byte b1, byte b2, byte b3) GetBytes(ActionContext context) => ((byte)Types.Actions.KeyPress,
            (byte)((ushort)Key & 0xff),
            (byte)(((ushort)Key & 0xff00) >> 8));

        [XmlAttribute("Key")]
        public Keys Key { get; set; }
    }
}
