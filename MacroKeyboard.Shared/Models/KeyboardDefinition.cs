﻿using MacroKeyboard.Shared.Helpers;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    [XmlRoot("Definition")]
    public class KeyboardDefinition : BaseModel
    {
        public override ValidationError? Validate(string definitionPath)
        {
            // Must have at least one screen

            if (Screens == null || !Screens.Any())
                return new ValidationError("Definition", "No screens defined!");

            // Must have up to 256 screens

            if (Screens.Count > ushort.MaxValue)
                return new ValidationError("Definition", $"Up to {ushort.MaxValue} screens are supported!");

            // Screen IDs must be unique

            HashSet<int> screenIds = new();
            for (int i = 0; i < Screens.Count; i++)
            {
                if (screenIds.Contains(Screens[i].Id))
                    return new ValidationError($"Definition.Screens[{i}]", $"Screen's Id ({Screens[i].Id}) is not unique.");

                screenIds.Add(Screens[i].Id);
            }

            if (!screenIds.Contains(0))
                return new ValidationError($"Definition", "A Screen with id 0 is required (this is the root screen)!");

            // Screen references must point to valid screen

            for (int i = 0; i < Screens.Count; i++)
            {
                var screen = Screens[i];

                for (int j = 0; j < screen.Items.Count; j++)
                {
                    if (screen.Items[j] is Item item)

                        for (int k = 0; k < item.Actions.Count; k++)
                        {
                            var action = item.Actions[k];

                            if (action is SwitchToScreenAction switchAction && !screenIds.Contains(switchAction.Id))
                                return new ValidationError($"Definition.Screen[{i}].Item[{j}].Action[{k}]", $"SwitchToScreen action points to non-existing screen Id ({switchAction.Id})!");
                        }
                }
            }

            // Validate individual screens

            for (int i = 0; i < Screens.Count; i++)
            {
                ValidationError? screenError = Screens[i].Validate(definitionPath);
                if (screenError != null)
                    return new ValidationError($"Definition.Screen[{i}]", "Error in screen", screenError);
            }

            // Everything is OK

            return null;
        }

        public void WriteToStream(string definitionPath, BinaryWriter outWriter, StreamWriter mapWriter, ref int offset, string mapIndent = "  ")
        {
            var context = new ActionContext(definitionPath);

            MapHelper.AddSection(mapWriter, $"{mapIndent}--- Definition ---");

            // Screen count
            outWriter.Write((byte)(Screens.Count & 0xff));
            outWriter.Write((byte)((Screens.Count & 0xff00) >> 8));
            MapHelper.AddEntry(mapWriter, offset, ((ushort)Screens.Count).ToString(), 2.ToString(), $"{mapIndent}Screen count");
            offset += 2;

            // Screens
            foreach (var screen in Screens)
            {
                screen.WriteToStream(outWriter, mapWriter, context, ref offset, $"  {mapIndent}");
            }

            // String resources

            MapHelper.AddSection(mapWriter, "--- String resources ---");

            outWriter.Write((byte)(context.StringResources.Count & 0xff));
            outWriter.Write((byte)((context.StringResources.Count & 0xff00) >> 8));
            MapHelper.AddEntry(mapWriter, offset, ((ushort)context.StringResources.Count).ToString(), 2.ToString(), $"{mapIndent}String resource count");
            offset += 2;

            ushort longest = (ushort)(context.StringResources.Any() ? context.StringResources.Max(x => x.Length) + 1 : 0);

            outWriter.Write((byte)(longest & 0xff));
            outWriter.Write((byte)((longest & 0xff00) >> 8));
            MapHelper.AddEntry(mapWriter, offset, longest.ToString(), 2.ToString(), $"{mapIndent}Longest resource (in chars)");
            offset += 2;

            for (int i = 0; i < context.StringResources.Count; i++)
            {
                string resource = context.StringResources[i];
                byte[] utf16Data = Encoding.Unicode.GetBytes(resource);

                outWriter.Write(utf16Data);
                outWriter.Write((ushort)0);

                int byteLength = utf16Data.Length + 2;
                MapHelper.AddEntry(mapWriter, offset, resource.Length > 13 ? $"{resource.Substring(0, 13)}..." : resource, byteLength.ToString(), $"{mapIndent}String resource");
                offset += byteLength;
            }
        }

        [XmlArray]
        [XmlArrayItem(ElementName = "Screen", Type = typeof(Screen))]
        public List<Screen> Screens { get; set; } = new();
    }
}
