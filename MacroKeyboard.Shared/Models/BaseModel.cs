﻿using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public abstract class BaseModel
    {
        public abstract ValidationError? Validate(string definitionPath);


    }
}
