﻿using MacroKeyboard.Shared.Types;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class ModifierDownAction : BaseAction
    {
        public override ValidationError? Validate(string definitionPath)
        {
            if (Modifier == 0)
                return new ValidationError("ModifierDown", $"Modifier {Modifier} does not match any know modifier name!");

            return null;
        }

        protected override string GetHeader() => "--- Modifier down action ---";

        protected override (byte b1, byte b2, byte b3) GetBytes(ActionContext context) => ((byte)Types.Actions.ModifierDown,
            (byte)((ushort)Modifier & 0xff),
            (byte)(((ushort)Modifier & 0xff00) >> 8));

        [XmlAttribute("Modifier")]
        public Modifiers Modifier { get; set; }
    }
}
