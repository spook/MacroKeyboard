﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public abstract class BaseSerializableItem : BaseModel
    {
        public abstract void WriteToStream(BinaryWriter outStream, StreamWriter mapWriter, ActionContext context, ref int offset, string mapIndent = "");
    }
}
