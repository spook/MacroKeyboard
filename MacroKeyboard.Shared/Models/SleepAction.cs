﻿using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public class SleepAction : BaseAction
    {
        public override ValidationError? Validate(string definitionPath)
        {
            return null;
        }

        protected override string GetHeader() => "--- Sleep action ---";

        protected override (byte b1, byte b2, byte b3) GetBytes(ActionContext context) => ((byte)Types.Actions.Sleep,
            0xff,
            0xff);
    }
}
