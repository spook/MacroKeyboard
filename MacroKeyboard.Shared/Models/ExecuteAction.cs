﻿using MacroKeyboard.Shared.Types;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class ExecuteAction : BaseStringAction
    {
        private string command;

        protected override Actions GetAction() => Actions.Execute;

        protected override string GetActionString() => command;

        protected override string GetHeader()
        {
            return "--- Execute action ---";
        }

        public override ValidationError? Validate(string definitionPath)
        {
            if (string.IsNullOrEmpty(command))
                return new ValidationError("ExecuteAction", "Command is not specified!");

            return null;
        }

        [XmlAttribute("Command")]
        public string Command
        {
            get => command;
            set => command = value;
        }
    }
}
