﻿using MacroKeyboard.Shared.Types;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class ActivateAction : BaseStringAction
    {
        private string binary;

        protected override Actions GetAction() => Actions.Activate;

        protected override string GetActionString() => binary;

        protected override string GetHeader()
        {
            return "--- Activate action ---";
        }

        public override ValidationError? Validate(string definitionPath)
        {
            if (string.IsNullOrEmpty(binary))
                return new ValidationError("ActivateAction", "Command is not specified!");

            return null;
        }

        [XmlAttribute("Binary")]
        public string Binary
        {
            get => binary;
            set => binary = value;
        }
    }
}
