﻿using MacroKeyboard.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public abstract class BaseAction : BaseSerializableItem
    {
        protected abstract (byte b1, byte b2, byte b3) GetBytes(ActionContext context);
        protected abstract string GetHeader();

        public int GetByteSize() => 3;

        public override sealed void WriteToStream(BinaryWriter outStream, StreamWriter mapWriter, ActionContext context, ref int offset, string mapIndent = "")
        {
            string header = GetHeader();
            MapHelper.AddSection(mapWriter, $"{mapIndent}{header}");

            (byte b1, byte b2, byte b3) = GetBytes(context);

            // Action code
            outStream.Write(b1);
            MapHelper.AddEntry(mapWriter, offset, b1.ToString(), 1.ToString(), $"{mapIndent}Byte 1 (Action type)");
            offset++;

            // Key code
            outStream.Write(b2);
            outStream.Write(b3);
            MapHelper.AddEntry(mapWriter, offset, $"{b2} {b3}", 2.ToString(), $"{mapIndent}Byte 2 (Action data)");
            offset++;
        }
    }
}
