﻿using MacroKeyboard.Shared.Models.Validation;
using MacroKeyboard.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class SwitchEncoderModeAction : BaseAction
    {
        public override ValidationError? Validate(string definitionPath)
        {
            var modes = (EncoderModes[])Enum.GetValues(typeof(EncoderModes));

            var minMode = modes.Select(m => (byte)m).Min();
            var maxMode = modes.Select(m => (byte)m).Max();

            if (Mode < minMode || Mode > maxMode)
                return new ValidationError("SwitchEncoderMode", $"Mode {Mode} is outside valid range [{minMode}..{maxMode}]");

            return null;
        }

        protected override string GetHeader() => "--- Switch to encoder mode action ---";

        protected override (byte b1, byte b2, byte b3) GetBytes(ActionContext context) => ((byte)Types.Actions.SwitchToEncoderMode,
            Mode,
            0xFF);

        [XmlAttribute("Mode")]
        public byte Mode { get; set; }
    }
}
