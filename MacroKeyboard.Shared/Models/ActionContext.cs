﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public class ActionContext
    {
        public ActionContext(string definitionPath)
        {
            DefinitionPath = definitionPath;
        }

        public string DefinitionPath { get; }
        public List<string> StringResources { get; } = new();
    }
}
