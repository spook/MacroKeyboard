﻿using MacroKeyboard.Shared.Helpers;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class Item : BaseItem
    {
        private const int ICON_WIDTH = 32;
        private const int ICON_HEIGHT = 32;

        public override ValidationError? Validate(string definitionPath)
        {
            if (Header != null && Header.Length > 127)
                return new ValidationError("Item", "Header exceeds 127 characters!");

            string fullIconPath = definitionPath != null ? System.IO.Path.Combine(System.IO.Path.GetDirectoryName(definitionPath), IconPath ?? String.Empty) : IconPath;

            if (string.IsNullOrEmpty(fullIconPath))
                return new ValidationError("Item", "IconPath is not defined.");

            if (!File.Exists(fullIconPath))
                return new ValidationError("Item", $"IconPath points to non-existing file ({IconPath}).");

            using (Bitmap bitmap = new Bitmap(fullIconPath))
            {
                if (bitmap.Width != ICON_WIDTH || bitmap.Height != ICON_HEIGHT)
                    return new ValidationError("Item", $"Icon {IconPath} does not match size requirements ({ICON_WIDTH}x{ICON_HEIGHT} pixels).");
            }

            if (Actions == null || Actions.Count == 0)
                return new ValidationError("Item", "There are no actions defined for item.");

            int actionBytes = Actions.Aggregate(0, (current, action) => current += action.GetByteSize());

            if (actionBytes > ushort.MaxValue)
                return new ValidationError("Item", $"Actions cannot occupy more than {ushort.MaxValue} bytes!");

            for (int i = 0; i < Actions.Count; i++)
            {
                var error = Actions[i].Validate(definitionPath);
                if (error != null)
                    return new ValidationError($"Item.Actions[{i}]", "Error in action", error);
            }

            return null;
        }

        public override void WriteToStream(BinaryWriter outStream, StreamWriter mapWriter, ActionContext context, ref int offset, string mapIndent = "")
        {
            MapHelper.AddSection(mapWriter, $"{mapIndent}--- Item ---");

            // Item type
            outStream.Write((byte)Types.Items.Item);
            MapHelper.AddEntry(mapWriter, offset, ((byte)Types.Items.Item).ToString(), 1.ToString(), $"{mapIndent}Item type");
            offset++;

            // Header length
            outStream.Write((byte)Header.Length);
            MapHelper.AddEntry(mapWriter, offset, ((byte)Header.Length).ToString(), 1.ToString(), $"{mapIndent}Header length");
            offset++;

            // Header
            byte[] data = Encoding.ASCII.GetBytes(Header);
            outStream.Write(data);
            MapHelper.AddEntry(mapWriter, offset, Header.Length > 13 ? Header.Substring(0, 13) + "..." : Header, Header.Length.ToString(), $"{mapIndent}Header");
            offset += data.Length;

            string fullIconPath = context.DefinitionPath != null ? System.IO.Path.Combine(System.IO.Path.GetDirectoryName(context.DefinitionPath), IconPath) : IconPath;

            using Bitmap originalIcon = new Bitmap(fullIconPath);
            using Bitmap icon = new Bitmap(originalIcon.Width, originalIcon.Height, PixelFormat.Format32bppPArgb);
            using Graphics gr = Graphics.FromImage(icon);

            gr.DrawImage(originalIcon, new Rectangle(0, 0, icon.Width, icon.Height));
            int bytesWritten = 0;

            for (int y = 0; y < ICON_HEIGHT; y++)
            {
                var line = icon.LockBits(new Rectangle(0, y, ICON_WIDTH, 1), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                var pixels = new byte[line.Stride];
                Marshal.Copy(line.Scan0, pixels, 0, line.Stride);

                for (int x = 0; x < ICON_WIDTH / 8; x++)
                {
                    byte outPixels = 0b11111111;

                    for (int i = 0; i < 8; i++)
                    {
                        byte b = pixels[x * 8 * 4 + 4 * i];
                        byte g = pixels[x * 8 * 4 + 4 * i + 1];
                        byte r = pixels[x * 8 * 4 + 4 * i + 2];
                        byte a = pixels[x * 8 * 4 + 4 * i + 3];

                        if (a < 128)
                        {
                            continue;
                        }

                        var min = Math.Min(r, Math.Min(g, b));
                        var max = Math.Max(r, Math.Max(g, b));
                        var lut = (min + max) / 2;

                        if (lut < 128)
                            outPixels &= (byte)~(1 << 7 - i);
                    }

                    outStream.Write(outPixels);
                    bytesWritten++;
                }

                icon.UnlockBits(line);
            }

            MapHelper.AddEntry(mapWriter, offset, "(pixels)", bytesWritten.ToString(), $"{mapIndent}Icon pixel data");
            offset += bytesWritten;

            // Action bytes

            int actionBytes = Actions.Aggregate(0, (current, action) => current += action.GetByteSize());
            outStream.Write((byte)(actionBytes & 0xff));
            outStream.Write((byte)((actionBytes & 0xff00) >> 8));
            MapHelper.AddEntry(mapWriter, offset, ((ushort)actionBytes).ToString(), 2.ToString(), $"{mapIndent}Action bytes");
            offset += 2;

            // Actions
            for (int i = 0; i < Actions.Count; i++)
            {
                Actions[i].WriteToStream(outStream, mapWriter, context, ref offset, $"  {mapIndent}");
            }
        }


        [XmlAttribute("Header")]
        public string? Header { get; set; }

        [XmlAttribute("Icon")]
        public string? IconPath { get; set; }

        [XmlArray]
        [XmlArrayItem(ElementName = "ModifierUp", Type = typeof(ModifierUpAction))]
        [XmlArrayItem(ElementName = "ModifierDown", Type = typeof(ModifierDownAction))]
        [XmlArrayItem(ElementName = "KeyPress", Type = typeof(KeyPressAction))]
        [XmlArrayItem(ElementName = "SwitchToScreen", Type = typeof(SwitchToScreenAction))]
        [XmlArrayItem(ElementName = "ToggleEncoderMode", Type = typeof(ToggleEncoderModeAction))]
        [XmlArrayItem(ElementName = "Sleep", Type = typeof(SleepAction))]
        [XmlArrayItem(ElementName = "SwitchToEncoderMode", Type = typeof(SwitchEncoderModeAction))]
        [XmlArrayItem(ElementName = "Execute", Type = typeof(ExecuteAction))]
        [XmlArrayItem(ElementName = "Activate", Type = typeof(ActivateAction))]
        public List<BaseAction> Actions { get; set; } = new();
    }
}
