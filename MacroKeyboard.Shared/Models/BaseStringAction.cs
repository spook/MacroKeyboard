﻿using MacroKeyboard.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public abstract class BaseStringAction : BaseAction
    {
        protected abstract Actions GetAction();

        protected abstract string GetActionString();

        protected override sealed (byte b1, byte b2, byte b3) GetBytes(ActionContext context)
        {
            string actionString = GetActionString();
            Actions actionType = GetAction();

            byte b1 = (byte)actionType;

            int index = context.StringResources.IndexOf(actionString);
            if (index == -1)
            {
                context.StringResources.Add(actionString);
                index = context.StringResources.Count - 1;
            }

            byte b2 = (byte)((ushort)index & 0xff);
            byte b3 = (byte)(((ushort)index & 0xff00) >> 8);

            return (b1, b2, b3);
        }
    }
}
