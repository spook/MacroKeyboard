﻿using MacroKeyboard.Shared.Helpers;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Shared.Models
{
    public class EmptyItem : BaseItem
    {
        public override ValidationError? Validate(string definitionPath)
        {
            return null;
        }

        public override void WriteToStream(BinaryWriter outStream, StreamWriter mapWriter, ActionContext context, ref int offset, string mapIndent = "")
        {
            MapHelper.AddSection(mapWriter, $"{mapIndent}--- EmptyItem ---");

            // Item type
            outStream.Write((byte)Types.Items.EmptyItem);
            MapHelper.AddEntry(mapWriter, offset, ((byte)Types.Items.EmptyItem).ToString(), 1.ToString(), $"{mapIndent}Item type");
            offset++;
        }
    }
}
