﻿using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class SwitchToScreenAction : BaseAction
    {
        public override ValidationError? Validate(string definitionPath)
        {
            return null;
        }

        protected override string GetHeader() => "--- Switch to screen action ---";

        protected override (byte b1, byte b2, byte b3) GetBytes(ActionContext context) => ((byte)Types.Actions.SwitchToScreen,
            (byte)(Id & 0xff),
            (byte)((Id & 0xff00) >> 8));

        [XmlAttribute("Id")]
        public ushort Id { get; set; }
    }
}
