﻿using MacroKeyboard.Shared.Helpers;
using MacroKeyboard.Shared.Models.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Shared.Models
{
    public class Screen : BaseSerializableItem
    {
        public override ValidationError? Validate(string definitionPath)
        {
            if (Items == null || !Items.Any())
                return new ValidationError("Screen", "Screen doesn't contain any items!");

            if (Items.Count > 12)
                return new ValidationError("Screen", "Screen supports only up to 12 items (= number of Macro Keyboard keys)");

            for (int i = 0; i < Items.Count; i++)
            {
                var error = Items[i].Validate(definitionPath);
                if (error != null)
                    return new ValidationError($"Screen.Items[{i}]", "Error in item!", error);
            }

            return null;
        }

        public override void WriteToStream(BinaryWriter outStream, StreamWriter mapWriter, ActionContext context, ref int offset, string mapIndent = "")
        {
            MapHelper.AddSection(mapWriter, $"{mapIndent}--- Screen ---");

            // Id

            outStream.Write((byte)(Id & 0xff));
            outStream.Write((byte)((Id & 0xff00) >> 8));
            MapHelper.AddEntry(mapWriter, offset, Id.ToString(), 2.ToString(), $"{mapIndent}Screen Id");
            offset += 2;

            // Item count

            outStream.Write((byte)(Items.Count & 0xff));
            outStream.Write((byte)((Items.Count & 0xff00) << 8));
            MapHelper.AddEntry(mapWriter, offset, ((byte)Items.Count).ToString(), 2.ToString(), $"{mapIndent}Item count");
            offset += 2;

            // Items

            for (int i = 0; i < Items.Count; i++)
            {
                Items[i].WriteToStream(outStream, mapWriter, context, ref offset, $"  {mapIndent}");
            }
        }

        [XmlAttribute("Id")]
        public byte Id { get; set; }

        [XmlArray]
        [XmlArrayItem(ElementName = "Item", Type = typeof(Item))]
        [XmlArrayItem(ElementName = "EmptyItem", Type = typeof(EmptyItem))]
        public List<BaseItem> Items { get; set; } = new();
    }
}
