﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboardGen
{
    public static class PathHelper
    {
        public static string? ChangeExtension(string? path, string newExtension)
        {
            if (path == null)
                return null;

            if (!newExtension.StartsWith('.'))
                newExtension = $".{newExtension}";

            var filePath = System.IO.Path.GetDirectoryName(path);
            var fileName = System.IO.Path.GetFileNameWithoutExtension(path);

            return Path.Combine(filePath, $"{fileName}{newExtension}");
        }
    }
}
