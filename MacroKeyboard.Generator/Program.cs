﻿using CommandLine;
using MacroKeyboard.Shared.Models;
using MacroKeyboardGen.Options;
using System;
using System.Xml;
using System.Xml.Serialization;

namespace MacroKeyboardGen
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<DemoOptions, GenerateOptions>(args)
                .WithParsed<DemoOptions>(opt => DemoCommand.Execute(opt))
                .WithParsed<GenerateOptions>(opt => GenerateCommand.Execute(opt));                                               
        }
    }
}