﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboardGen
{
    public static class ConsoleHelper
    {
        public static void ShowError(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Error: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(error);
        }

        public static void TerminateWithError(string error)
        {
            ShowError(error);
            Environment.Exit(1);
        }
    }
}
