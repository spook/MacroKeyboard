﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboardGen.Options
{
    [Verb("generate", HelpText = "Builds binary definition for MacroKeybard")]
    public class GenerateOptions
    {
        [Option("input", HelpText = "Input XML file", Required = true)]
        public string? InputFile { get; set; }

        [Option("output", HelpText = "Output binary file", Required = false)]
        public string? OutputFile { get; set; }

        [Option("map", HelpText = "Output map file", Required = false)]
        public string? MapFile { get; set; }
    }
}
