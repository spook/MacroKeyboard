﻿using MacroKeyboard.Shared.Models;
using MacroKeyboard.Shared.Types;
using MacroKeyboardGen.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MacroKeyboardGen
{
    public static class DemoCommand
    {
        public static void Execute(DemoOptions _)
        {
            // Generate demo definition containing all possible options

            var def = new KeyboardDefinition
            {
                Screens = new List<Screen>
                {
                    new Screen
                    {
                        Id = 0,
                        Items = new List<BaseItem>
                        {
                            new Item
                            {
                                Header = "Item1",
                                IconPath = "Icon1.png",
                                Actions = new List<BaseAction>
                                {
                                    new KeyPressAction { Key = Keys.F5 }
                                }
                            },

                            new Item
                            {
                                Header = "Item2",
                                IconPath = "Icon2.png",
                                Actions = new List<BaseAction>
                                {
                                    new ModifierDownAction { Modifier = Modifiers.LeftShift },
                                    new KeyPressAction { Key = Keys.F1 },
                                    new ModifierUpAction { Modifier = Modifiers.LeftShift }
                                }
                            },

                            new Item
                            {
                                Header = "Item3",
                                IconPath = "Icon3.png",
                                Actions = new List<BaseAction>
                                {
                                    new SwitchToScreenAction { Id = 1 }
                                }
                            },

                            new EmptyItem(),

                            new EmptyItem()
                        }
                    },

                    new Screen
                    {
                        Id = 1,
                        Items = new List<BaseItem>
                        {
                            new Item
                            {
                                Header = "Item1",
                                IconPath = "Icon4.png",
                                Actions = new List<BaseAction>
                                {
                                    new ToggleEncoderModeAction()
                                }
                            },

                            new Item
                            {
                                Header = "Item2",
                                IconPath = "Icon5.png",
                                Actions = new List<BaseAction>
                                {
                                    new SwitchToScreenAction { Id = 0 }
                                }
                            }
                        }
                    }
                }
            };

            var serializer = new XmlSerializer(typeof(KeyboardDefinition));

            using (StringWriter textWriter = new StringWriter())
            using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, new XmlWriterSettings { Indent = true }))
            {
                serializer.Serialize(xmlWriter, def);
                Console.WriteLine(textWriter.ToString());
            }
        }
    }
}
