﻿using MacroKeyboard.Shared.Models;
using MacroKeyboard.Shared.Models.Validation;
using MacroKeyboardGen.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboardGen
{
    public static class GenerateCommand
    {
        public static void Execute(GenerateOptions opt)
        {
            // Validate options
            
            if (!File.Exists(opt.InputFile))
                ConsoleHelper.TerminateWithError("Input file does not exist!");

            // Fill missing parameters

            if (string.IsNullOrEmpty(opt.OutputFile))
            {
                if (Path.GetExtension(opt.InputFile ?? String.Empty).ToLower() != ".def")
                    opt.OutputFile = PathHelper.ChangeExtension(opt.InputFile, ".def");
                else
                    opt.OutputFile = PathHelper.ChangeExtension(opt.InputFile, ".outdef");
            }

            if (string.IsNullOrEmpty(opt.MapFile))
            {
                if (Path.GetExtension(opt.InputFile ?? String.Empty).ToLower() != ".map")
                    opt.MapFile = PathHelper.ChangeExtension(opt.InputFile, ".map");
                else
                    opt.MapFile = PathHelper.ChangeExtension(opt.InputFile, ".outmap");
            }

            // Load XML

            KeyboardDefinition definition;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(KeyboardDefinition));
                using (FileStream fs = new FileStream(opt.InputFile, FileMode.Open, FileAccess.Read))
                {
                    definition = (KeyboardDefinition)serializer.Deserialize(fs);
                }
            }
            catch (Exception e)
            {
                ConsoleHelper.TerminateWithError($"Failed to read input file {opt.InputFile}: {e.Message}");
                return;
            }

            // Validate definition

            ValidationError validationErrors = definition.Validate(opt.InputFile);
            if (validationErrors != null)
            {
                ConsoleHelper.ShowError("Definition validation failed. Details:\r\n");

                Console.WriteLine(validationErrors.ToString());

                Environment.Exit(1);
            }

            // Build binary definition

            int offset = 0;
            using (FileStream outFs = new FileStream(opt.OutputFile, FileMode.Create, FileAccess.Write))
            using (BinaryWriter outWriter = new BinaryWriter(outFs))
            using (FileStream mapFs = new FileStream(opt.MapFile, FileMode.Create, FileAccess.Write))
            using (StreamWriter mapWriter = new StreamWriter(mapFs))
                definition.WriteToStream(opt.InputFile, outWriter, mapWriter, ref offset);
        }
    }
}