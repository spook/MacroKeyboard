﻿using MacroKeyboard.Editor.BusinessLogic.Services.Dialogs;
using MacroKeyboard.Editor.BusinessLogic.Services.Messaging;
using MacroKeyboard.Editor.BusinessLogic.ViewModels.Base;
using MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition;
using MacroKeyboard.Editor.Resources;
using MacroKeyboard.Shared.Models;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Main
{
    public class MainWindowViewModel : BaseViewModel
    {
        // Private fields -----------------------------------------------------

        private readonly IMainWindowAccess access;
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private string errorLog;

        private DefinitionEditorViewModel definition;

        private void InternalSaveDefinition(string path)
        {
            var model = definition.BuildModel();

            XmlSerializer serializer = new(typeof(KeyboardDefinition));
            using var fs = new FileStream(path, FileMode.Create, FileAccess.Write);

            serializer.Serialize(fs, model);
        }

        private void InternalOpenDefinition(string path)
        {
            XmlSerializer serializer = new(typeof(KeyboardDefinition));
            using var fs = new FileStream(path, FileMode.Open, FileAccess.Read);

            Definition?.Dispose();

            var definition = (KeyboardDefinition)serializer.Deserialize(fs);
            Definition = new DefinitionEditorViewModel(definition, path, dialogService, messagingService);
        }

        private void DoNewDefinition()
        {
            Definition?.Dispose();

            Definition = new DefinitionEditorViewModel(dialogService, messagingService);
        }

        private void DoSaveDefinition()
        {
            if (String.IsNullOrEmpty(Definition.Path))
                DoSaveDefinitionAs();
            else
                InternalSaveDefinition(Definition.Path);
        }

        private void DoOpenDefinition()
        {
            (bool result, string path) = dialogService.ShowOpenDialog(Strings.DefinitionFilter);

            if (result)
            {
                InternalOpenDefinition(path);
            }
        }

        private void DoSaveDefinitionAs()
        {
            (bool result, string path) = dialogService.ShowSaveDialog(Strings.DefinitionFilter, filename: Definition.Path);

            if (result)
            {
                InternalSaveDefinition(path);
                Definition.Path = path;
            }
        }

        private void DoGenerate()
        {
            var model = Definition.BuildModel();
            var error = model.Validate(Definition.Path);

            if (error != null)
            {
                StringBuilder fullError = new();

                var msg = error;
                while (error != null)
                {
                    fullError.Append(error.Path);
                    fullError.Append(": ");
                    fullError.AppendLine(error.Message);
                    error = error.Inner;
                }

                ErrorLog = fullError.ToString();
                messagingService.ShowError(Strings.Message_ValidationFailed);
                return;
            }
            else
            {
                ErrorLog = null;
            }

            (bool result, string path) = dialogService.ShowSaveDialog(MacroKeyboard.Editor.Resources.Strings.Filter_BinaryKeyboardDefinition, filename: System.IO.Path.Combine(Path.GetDirectoryName(Definition.Path), "Keyboard.def"));
            if (result)
            {
                string mapPath = System.IO.Path.ChangeExtension(path, ".map");

                int offset = 0;
                using (FileStream outFs = new FileStream(path, FileMode.Create, FileAccess.Write))
                using (BinaryWriter outWriter = new BinaryWriter(outFs))
                using (FileStream mapFs = new FileStream(mapPath, FileMode.Create, FileAccess.Write))
                using (StreamWriter mapWriter = new StreamWriter(mapFs))
                    model.WriteToStream(Definition.Path, outWriter, mapWriter, ref offset);
            }
        }


        // Public methods -----------------------------------------------------

        public MainWindowViewModel(IMainWindowAccess access, IDialogService dialogService, IMessagingService messagingService)
        {
            this.access = access;
            this.dialogService = dialogService;

            var definitionExistsCondition = new LambdaCondition<MainWindowViewModel>(this, vm => vm.Definition != null, false);

            NewDefinitionCommand = new AppCommand(obj => DoNewDefinition());
            OpenDefinitionCommand = new AppCommand(obj => DoOpenDefinition());
            SaveDefinitionCommand = new AppCommand(obj => DoSaveDefinition(), definitionExistsCondition);
            SaveDefinitionAsCommand = new AppCommand(obj => DoSaveDefinitionAs(), definitionExistsCondition);
            GenerateDefinitionCommand = new AppCommand(obj => DoGenerate(), definitionExistsCondition);
            this.messagingService = messagingService;
        }

        // Public properties --------------------------------------------------

        public DefinitionEditorViewModel Definition 
        { 
            get => definition;
            set => Set(ref definition, value);
        }

        public string ErrorLog
        {
            get => errorLog;
            set => Set(ref errorLog, value);
        }

        public ICommand NewDefinitionCommand { get; }
        public ICommand OpenDefinitionCommand { get; }
        public ICommand SaveDefinitionCommand { get; }
        public ICommand SaveDefinitionAsCommand { get; }
        public ICommand GenerateDefinitionCommand { get; }
    }
}
