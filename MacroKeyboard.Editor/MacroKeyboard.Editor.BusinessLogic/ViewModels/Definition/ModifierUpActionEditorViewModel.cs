﻿using MacroKeyboard.Shared.Models;
using MacroKeyboard.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class ModifierUpActionEditorViewModel : BaseActionEditorViewModel
    {
        private Modifiers modifier;

        public ModifierUpActionEditorViewModel(ModifierUpAction action)
        {
            try
            {
                modifier = action.Modifier;
            }
            catch
            {
                modifier = Modifiers.LeftShift;
            }
        }

        public ModifierUpActionEditorViewModel()
        {
            modifier = Modifiers.LeftShift;
        }

        public override BaseAction BuildModel()
        {
            return new ModifierUpAction
            {
                Modifier = modifier
            };
        }

        public Modifiers Modifier
        {
            get => modifier;
            set => Set(ref modifier, value);
        }
    }
}
