﻿using MacroKeyboard.Shared.Models;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class ExecuteActionEditorViewModel : BaseActionEditorViewModel
    {
        private string path;

        public ExecuteActionEditorViewModel(ExecuteAction executeAction)
        {
            path = executeAction.Command;
        }

        public ExecuteActionEditorViewModel()
        {
            path = null;
        }

        public override BaseAction BuildModel()
        {
            var result = new ExecuteAction();
            result.Command = path;
            return result;
        }

        public string Path
        {
            get => path;
            set => Set(ref path, value);
        }
    }
}