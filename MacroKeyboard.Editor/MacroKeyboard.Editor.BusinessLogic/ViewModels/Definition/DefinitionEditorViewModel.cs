﻿using MacroKeyboard.Editor.BusinessLogic.Services.Dialogs;
using MacroKeyboard.Editor.BusinessLogic.Services.Messaging;
using MacroKeyboard.Editor.BusinessLogic.ViewModels.Base;
using MacroKeyboard.Editor.Resources;
using MacroKeyboard.Shared.Models;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class DefinitionEditorViewModel : BaseViewModel, IScreenHandler, IDisposable
    {
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private ScreenEditorViewModel selectedScreen;

        public KeyboardDefinition BuildModel()
        {
            var result = new KeyboardDefinition();

            result.Screens = Screens.Select(s => s.BuildModel()).ToList();

            return result;
        }

        private void DoAddScreen()
        {
            byte newId = (byte)((!Screens.Any()) ? 0 : Screens.Max(s => s.Id) + 1);
            var newScreen = new ScreenEditorViewModel(newId, this, dialogService, messagingService);
            Screens.Add(newScreen);
            SelectedScreen = newScreen;
        }

        private void DoDeleteScreen()
        {
            string message = String.Format(Strings.Message_ScreenDeleteConfirmation, SelectedScreen.Id);
            if (messagingService.AskYesNo(message))
            {
                int oldIndex = Screens.IndexOf(SelectedScreen);

                var oldScreen = Screens[oldIndex];
                Screens.RemoveAt(oldIndex);
                oldScreen.Dispose();

                oldIndex = Math.Max(0, Math.Min(oldIndex, Screens.Count - 1));
                if (Screens.Count > 0)
                    SelectedScreen = Screens[oldIndex];
            }
        }

        private void BuildCommands()
        {
            var screenSelectedCondition = new LambdaCondition<DefinitionEditorViewModel>(this, vm => vm.SelectedScreen != null, false);

            AddScreenCommand = new AppCommand(obj => DoAddScreen());
            DeleteScreenCommand = new AppCommand(obj => DoDeleteScreen(), screenSelectedCondition);
        }

        public DefinitionEditorViewModel(KeyboardDefinition definition, string path, IDialogService dialogService, IMessagingService messagingService)
        {
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            Screens = new ObservableCollection<ScreenEditorViewModel>();
            Path = path;

            if (definition != null && definition.Screens != null)
            {
                foreach (var screen in definition?.Screens)
                {
                    var screenViewModel = new ScreenEditorViewModel(screen, this, dialogService, messagingService);
                    Screens.Add(screenViewModel);
                }
            }

            BuildCommands();
        }

        public DefinitionEditorViewModel(IDialogService dialogService, IMessagingService messagingService)
        {
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            Screens = new ObservableCollection<ScreenEditorViewModel>();
            Path = null;

            BuildCommands();
        }

        public void Dispose()
        {
            foreach (var screen in Screens)
                screen.Dispose();
        }

        public ObservableCollection<ScreenEditorViewModel> Screens { get; }

        public ScreenEditorViewModel SelectedScreen
        {
            get => selectedScreen;
            set => Set(ref selectedScreen, value);
        }

        public string Path { get; set; }

        public ICommand AddScreenCommand { get; private set; }
        public ICommand DeleteScreenCommand { get; private set; }
    }
}
