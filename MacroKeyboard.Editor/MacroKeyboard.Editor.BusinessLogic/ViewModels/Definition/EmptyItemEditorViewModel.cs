﻿using MacroKeyboard.Shared.Models;
using Spooksoft.VisualStateManager.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class EmptyItemEditorViewModel : BaseItemEditorViewModel
    {
        private void DoSwitchToItem()
        {
            handler.SwitchToItem(this);
        }

        public EmptyItemEditorViewModel(IItemEditorHandler handler) 
            : base(handler)
        {
            SwitchToItemCommand = new AppCommand(obj => DoSwitchToItem());
        }

        public override BaseItem BuildModel()
        {
            return new EmptyItem();
        }

        public ICommand SwitchToItemCommand { get; }
    }
}
