﻿using MacroKeyboard.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class ToggleEncoderModeActionEditorViewModel : BaseActionEditorViewModel
    {
        public ToggleEncoderModeActionEditorViewModel(ToggleEncoderModeAction toggleEncoderModeAction)
        {

        }

        public ToggleEncoderModeActionEditorViewModel()
        {

        }

        public override BaseAction BuildModel()
        {
            return new ToggleEncoderModeAction();
        }
    }
}
