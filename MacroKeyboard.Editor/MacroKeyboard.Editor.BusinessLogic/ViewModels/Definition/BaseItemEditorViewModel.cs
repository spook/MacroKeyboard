﻿using MacroKeyboard.Editor.BusinessLogic.ViewModels.Base;
using MacroKeyboard.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public abstract class BaseItemEditorViewModel : BaseViewModel, IDisposable
    {
        private bool selected;

        protected readonly IItemEditorHandler handler;

        private void HandleSelectedChanged()
        {
            if (selected)
                handler.NotifySelected(this);
        }

        public BaseItemEditorViewModel(IItemEditorHandler handler)
        {
            this.handler = handler;
        }

        public virtual void Dispose()
        {

        }

        public abstract BaseItem BuildModel();

        public bool Selected
        {
            get => selected;
            set => Set(ref selected, value, changeHandler: HandleSelectedChanged);
        }
    }
}
