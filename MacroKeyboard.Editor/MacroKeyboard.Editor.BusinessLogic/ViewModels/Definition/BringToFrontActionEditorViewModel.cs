﻿using MacroKeyboard.Shared.Models;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class BringToFrontActionEditorViewModel : BaseActionEditorViewModel
    {
        private string path;

        public BringToFrontActionEditorViewModel(ActivateAction bringToFrontAction)
        {
            path = bringToFrontAction.Binary;
        }

        public BringToFrontActionEditorViewModel()
        {
            path = null;
        }

        public override BaseAction BuildModel()
        {
            var result = new ActivateAction();
            result.Binary = Path;
            return result;
        }

        public string Path
        {
            get => path;
            set => Set(ref path, value);
        }
    }
}