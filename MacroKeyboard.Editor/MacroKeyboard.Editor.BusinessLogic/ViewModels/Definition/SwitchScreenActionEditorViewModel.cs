﻿using MacroKeyboard.Shared.Models;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class SwitchScreenActionEditorViewModel : BaseActionEditorViewModel
    {
        private ushort targetId;

        public SwitchScreenActionEditorViewModel(SwitchToScreenAction switchScreenAction)
        {
            targetId = switchScreenAction.Id;
        }

        public SwitchScreenActionEditorViewModel()
        {
            targetId = 0;
        }

        public override BaseAction BuildModel()
        {
            var result = new SwitchToScreenAction();
            result.Id = targetId;
            return result;
        }

        public ushort TargetId
        {
            get => targetId;
            set => Set(ref targetId, value);
        }
    }
}