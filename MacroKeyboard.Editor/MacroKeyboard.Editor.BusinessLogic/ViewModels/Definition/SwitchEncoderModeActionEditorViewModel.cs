﻿using MacroKeyboard.Shared.Models;
using MacroKeyboard.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class SwitchEncoderModeActionEditorViewModel : BaseActionEditorViewModel
    {
        private EncoderModes mode;

        public SwitchEncoderModeActionEditorViewModel(SwitchEncoderModeAction switchEncoderModeAction)
        {            
            mode = (EncoderModes)switchEncoderModeAction.Mode;
        }

        public SwitchEncoderModeActionEditorViewModel()
        {
            mode = EncoderModes.Arrows;
        }

        public override BaseAction BuildModel()
        {
            return new SwitchEncoderModeAction
            {
                Mode = (byte)mode
            };
        }

        public EncoderModes Mode
        {
            get => mode;
            set => Set(ref mode, value);
        }
    }
}
