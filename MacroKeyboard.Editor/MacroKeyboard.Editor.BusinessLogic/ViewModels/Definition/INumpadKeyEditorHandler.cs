﻿using System.ComponentModel;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public interface IItemEditorHandler : INotifyPropertyChanged
    {
        void NotifySelected(BaseItemEditorViewModel viewModel);
        void SwitchToEmpty(ItemEditorViewModel itemEditorViewModel);
        void SwitchToItem(EmptyItemEditorViewModel itemEditorViewModel);

        public string Path { get; }
    }
}