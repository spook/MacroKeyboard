﻿using MacroKeyboard.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class SleepActionEditorViewModel : BaseActionEditorViewModel
    {
        public override BaseAction BuildModel()
        {
            return new SleepAction();
        }
    }
}
