﻿using MacroKeyboard.Shared.Models;
using MacroKeyboard.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class KeyPressActionEditorViewModel : BaseActionEditorViewModel
    {
        private Keys key;

        public KeyPressActionEditorViewModel(KeyPressAction action)
        {
            try
            {
                key = action.Key;
            }
            catch
            {
                key = Keys.Space;
            }
        }

        public KeyPressActionEditorViewModel()
        {
            key = Keys.Space;
        }

        public override BaseAction BuildModel()
        {
            return new KeyPressAction
            {
                Key = key
            };
        }

        public Keys Key
        {
            get => key;
            set => Set(ref key, value);
        }
    }
}
