﻿using MacroKeyboard.Editor.BusinessLogic.Services.Dialogs;
using MacroKeyboard.Editor.BusinessLogic.Services.Messaging;
using MacroKeyboard.Editor.BusinessLogic.ViewModels.Base;
using MacroKeyboard.Editor.Resources;
using MacroKeyboard.Shared.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class ScreenEditorViewModel : BaseViewModel, IItemEditorHandler, IDisposable
    {
        private byte id;
        private string header;
        private BaseItemEditorViewModel selectedItem;
        private readonly IScreenHandler handler;
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;

        void IItemEditorHandler.NotifySelected(BaseItemEditorViewModel viewModel)
        {
            SelectedItem = viewModel;
        }

        private void HandleDefinitionPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(DefinitionEditorViewModel.Path))
            {
                OnPropertyChanged(nameof(Path));
            }
        }

        public ScreenEditorViewModel(Screen screen, IScreenHandler handler, IDialogService dialogService, IMessagingService messagingService)
        {
            this.id = screen.Id;
            this.handler = handler;
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            Items = new ObservableCollection<BaseItemEditorViewModel>();
            if (screen.Items.Count > 0)
            {
                for (int i = 0; i < screen.Items.Count; i++)
                {
                    BaseItemEditorViewModel itemEditor;
                    if (screen.Items[i] is Item item)
                        itemEditor = new ItemEditorViewModel(item, this, dialogService, messagingService);
                    else if (screen.Items[i] is EmptyItem emptyItem)
                        itemEditor = new EmptyItemEditorViewModel(this);
                    else
                        throw new InvalidOperationException("Unsupported item kind!");

                    Items.Add(itemEditor);
                }

                while (Items.Count < 12)
                    Items.Add(new EmptyItemEditorViewModel(this));
            }

            handler.PropertyChanged += HandleDefinitionPropertyChanged;
        }

        public ScreenEditorViewModel(byte newId, IScreenHandler handler, IDialogService dialogService, IMessagingService messagingService)
        {
            this.id = newId;
            this.dialogService = dialogService;
            this.messagingService = messagingService;
            this.handler = handler;

            Items = new ObservableCollection<BaseItemEditorViewModel>();

            for (int i = 0; i < 12; i++)
                Items.Add(new EmptyItemEditorViewModel(this));

            handler.PropertyChanged += HandleDefinitionPropertyChanged;
        }

        public void Dispose()
        {
            handler.PropertyChanged -= HandleDefinitionPropertyChanged;

            foreach (var item in Items)
                item.Dispose();
        }

        public Screen BuildModel()
        {
            var result = new Screen();
            result.Id = this.Id;
            result.Items = Items.Select(k => k.BuildModel()).ToList();

            return result;
        }

        public void SwitchToEmpty(ItemEditorViewModel viewModel)
        {
            int index = Items.IndexOf(viewModel);
            if (index == -1)
                return;

            var oldItem = Items[index];
            Items.RemoveAt(index);
            oldItem.Dispose();

            var empty = new EmptyItemEditorViewModel(this);
            Items.Insert(index, empty);
            empty.Selected = true;
        }

        public void SwitchToItem(EmptyItemEditorViewModel viewModel)
        {
            int index = Items.IndexOf(viewModel);
            if (index == -1)
                return;

            var oldItem = Items[index];
            Items.RemoveAt(index);
            oldItem.Dispose();

            var item = new ItemEditorViewModel(this, dialogService, messagingService);
            Items.Insert(index, item);
            item.Selected = true;
        }

        public byte Id
        {
            get => id;
            set => Set(ref id, value);
        }

        public ObservableCollection<BaseItemEditorViewModel> Items { get; }

        public BaseItemEditorViewModel SelectedItem
        {
            get => selectedItem;
            set => Set(ref selectedItem, value);
        }

        public string Path => System.IO.Path.GetDirectoryName(handler.Path ?? string.Empty);
    }
}