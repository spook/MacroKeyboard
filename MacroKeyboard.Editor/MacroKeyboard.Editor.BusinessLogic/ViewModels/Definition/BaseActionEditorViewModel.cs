﻿using MacroKeyboard.Editor.BusinessLogic.ViewModels.Base;
using MacroKeyboard.Shared.Models;
using System;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public abstract class BaseActionEditorViewModel : BaseViewModel
    {
        public static BaseActionEditorViewModel From(BaseAction action)
        {
            if (action is ModifierDownAction modifierDownAction)
            {
                return new ModifierDownActionEditorViewModel(modifierDownAction);
            }
            else if (action is ModifierUpAction modifierUpAction)
            {
                return new ModifierUpActionEditorViewModel(modifierUpAction);
            }
            else if (action is KeyPressAction keyPressAction)
            {
                return new KeyPressActionEditorViewModel(keyPressAction);
            }
            else if (action is ExecuteAction executeAction)
            {
                return new ExecuteActionEditorViewModel(executeAction);
            }
            else if (action is ActivateAction bringToFrontAction)
            {
                return new BringToFrontActionEditorViewModel(bringToFrontAction);
            }
            else if (action is SwitchToScreenAction switchScreenAction)
            {
                return new SwitchScreenActionEditorViewModel(switchScreenAction);
            }
            else if (action is ToggleEncoderModeAction toggleEncoderModeAction)
            {
                return new ToggleEncoderModeActionEditorViewModel(toggleEncoderModeAction);
            }
            else if (action is SwitchEncoderModeAction switchEncoderModeAction)
            {
                return new SwitchEncoderModeActionEditorViewModel(switchEncoderModeAction);
            }
            else
                throw new InvalidOperationException("Unsupported action!");
        }

        public abstract BaseAction BuildModel();
    }
}