﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public interface IScreenHandler : INotifyPropertyChanged
    {
        public string Path { get; set; }
    }
}
