﻿using MacroKeyboard.Editor.BusinessLogic.Services.Dialogs;
using MacroKeyboard.Editor.BusinessLogic.Services.Messaging;
using MacroKeyboard.Editor.BusinessLogic.ViewModels.Base;
using MacroKeyboard.Editor.Resources;
using MacroKeyboard.Shared.Models;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MacroKeyboard.Editor.BusinessLogic.ViewModels.Definition
{
    public class ItemEditorViewModel : BaseItemEditorViewModel
    {
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private string iconPath;
        private ImageSource iconSource;
        private string title;
        private BaseActionEditorViewModel selectedAction;
        private int selectedActionIndex;

        private void HandleIconPathChanged()
        {
            UpdateIconSource();
        }

        private void ClearIcon()
        {
            if (iconSource != null)
            {
                iconSource = null;
                OnPropertyChanged(nameof(IconSource));
            }
        }

        private void UpdateIconSource()
        {
            if (handler.Path == null)
            {
                ClearIcon();
                return;
            }

            string fullIconPath = System.IO.Path.Combine(handler.Path ?? string.Empty, iconPath ?? string.Empty);
            if (File.Exists(fullIconPath))
            {
                try
                {                    
                    iconSource = new BitmapImage(new Uri(fullIconPath));
                    OnPropertyChanged(nameof(IconSource));
                }
                catch
                {
                    ClearIcon();
                }
            }
            else
            {
                ClearIcon();
            }
        }

        private void DoDeleteAction()
        {
            if (messagingService.AskYesNo(Strings.Message_ActionDeleteConfirmation))
            {
                int actionIndex = Actions.IndexOf(SelectedAction);

                Actions.RemoveAt(actionIndex);
                actionIndex = Math.Max(0, Math.Min(Actions.Count - 1, actionIndex));
                if (Actions.Any())
                    SelectedAction = Actions[actionIndex];
            }
        }

        private void DoMoveActionDown()
        {
            Actions.Move(SelectedActionIndex, SelectedActionIndex + 1);
        }

        private void DoMoveActionUp()
        {
            Actions.Move(SelectedActionIndex, SelectedActionIndex - 1);
        }

        private void DoAddSwitchToScreen()
        {
            var newAction = new SwitchScreenActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddBringToFront()
        {
            var newAction = new BringToFrontActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddExecute()
        {
            var newAction = new ExecuteActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddModifierUp()
        {
            var newAction = new ModifierUpActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddKeyPress()
        {
            var newAction = new KeyPressActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddModifierDown()
        {
            var newAction = new ModifierDownActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;

        }

        private void DoAddToggleEncoderMode()
        {
            var newAction = new ToggleEncoderModeActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddSetEncoderMode()
        {
            var newAction = new SwitchEncoderModeActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddSleep()
        {
            var newAction = new SleepActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoSwitchToEmptyItem()
        {
            handler.SwitchToEmpty(this);
        }

        private void InitializeCommands()
        {
            AddExecuteActionCommand = new AppCommand(obj => DoAddExecute());
            AddBringToFrontActionCommand = new AppCommand(obj => DoAddBringToFront());
            AddSwitchToScreenCommand = new AppCommand(obj => DoAddSwitchToScreen());
            AddModifierDownActionCommand = new AppCommand(obj => DoAddModifierDown());
            AddKeyPressActionCommand = new AppCommand(obj => DoAddKeyPress());
            AddModifierUpActionCommand = new AppCommand(obj => DoAddModifierUp());
            AddToggleEncoderModeActionCommand = new AppCommand(obj => DoAddToggleEncoderMode());
            AddSetEncoderModeActionCommand = new AppCommand(obj => DoAddSetEncoderMode());
            AddSleepActionCommand = new AppCommand(obj => DoAddSleep());
            SwitchToEmptyItemCommand = new AppCommand(obj => DoSwitchToEmptyItem());

            var firstItemSelectedCondition = new LambdaCondition<ItemEditorViewModel>(this, vm => vm.SelectedActionIndex == 0, false);
            var lastItemSelectedCondition = new LambdaCondition<ItemEditorViewModel>(this, vm => vm.SelectedActionIndex == vm.Actions.Count - 1, false);
            var anyItemSelectedCondition = new LambdaCondition<ItemEditorViewModel>(this, vm => vm.SelectedAction != null, false);

            MoveActionUpCommand = new AppCommand(obj => DoMoveActionUp(), anyItemSelectedCondition & !firstItemSelectedCondition);
            MoveActionDownCommand = new AppCommand(obj => DoMoveActionDown(), anyItemSelectedCondition & !lastItemSelectedCondition);
            DeleteActionCommand = new AppCommand(obj => DoDeleteAction(), anyItemSelectedCondition);
        }

        private void HandleScreenPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ScreenEditorViewModel.Path))
            {
                UpdateIconSource();
            }
        }

        // Public methods -----------------------------------------------------

        public ItemEditorViewModel(Item item,
            IItemEditorHandler handler,
            IDialogService dialogService,
            IMessagingService messagingService)
            : base(handler)
        {
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            iconPath = item.IconPath;
            title = item.Header;

            Actions = new ObservableCollection<BaseActionEditorViewModel>();
            if (item.Actions != null && item.Actions.Count > 0)
            {
                for (int i = 0; i < item.Actions.Count; i++)
                {
                    var action = BaseActionEditorViewModel.From(item.Actions[i]);
                    Actions.Add(action);
                }
            }

            InitializeCommands();

            UpdateIconSource();

            handler.PropertyChanged += HandleScreenPropertyChanged;
        }

        public ItemEditorViewModel(IItemEditorHandler handler,
            IDialogService dialogService,
            IMessagingService messagingService)
            : base(handler)
        {
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            iconPath = null;
            title = null;

            Actions = new ObservableCollection<BaseActionEditorViewModel>();

            InitializeCommands();

            UpdateIconSource();

            handler.PropertyChanged += HandleScreenPropertyChanged;
        }

        public override BaseItem BuildModel()
        {
            var result = new Item();
            result.IconPath = iconPath;
            result.Header = title;
            result.Actions = Actions.Select(a => a.BuildModel()).ToList();

            return result;
        }

        public override void Dispose()
        {
            handler.PropertyChanged -= HandleScreenPropertyChanged;
        }

        // Public properties --------------------------------------------------

        public string IconPath
        {
            get => iconPath;
            set => Set(ref iconPath, value, changeHandler: HandleIconPathChanged);
        }

        public ImageSource IconSource
        {
            get => iconSource;
        }

        public string Title
        {
            get => title;
            set => Set(ref title, value);
        }

        public ObservableCollection<BaseActionEditorViewModel> Actions { get; }

        public BaseActionEditorViewModel SelectedAction
        {
            get => selectedAction;
            set => Set(ref selectedAction, value);
        }

        public int SelectedActionIndex
        {
            get => selectedActionIndex;
            set => Set(ref selectedActionIndex, value);
        }

        public ICommand AddExecuteActionCommand { get; private set; }
        public ICommand AddBringToFrontActionCommand { get; private set; }
        public ICommand AddSwitchToScreenCommand { get; private set; }
        public ICommand AddModifierDownActionCommand { get; private set; }
        public ICommand AddToggleEncoderModeActionCommand { get; private set; }
        public ICommand AddSetEncoderModeActionCommand { get; private set; }
        public ICommand SwitchToEmptyItemCommand { get; private set; }
        public ICommand AddKeyPressActionCommand { get; private set; }
        public ICommand AddModifierUpActionCommand { get; private set; }
        public ICommand AddSleepActionCommand { get; private set; }
        public ICommand MoveActionUpCommand { get; private set; }
        public ICommand MoveActionDownCommand { get; private set; }
        public ICommand DeleteActionCommand { get; private set; }
    }
}