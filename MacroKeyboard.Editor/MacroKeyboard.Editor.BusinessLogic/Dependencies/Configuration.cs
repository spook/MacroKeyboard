﻿using Autofac;
using MacroKeyboard.Editor.BusinessLogic.Services.EventBus;
using MacroKeyboard.Editor.BusinessLogic.Services.Paths;
using MacroKeyboard.Editor.BusinessLogic.ViewModels.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.BusinessLogic.Dependencies
{
    public static class Configuration
    {
        private static bool isConfigured = false;

        public static void Configure(ContainerBuilder builder)
        {
            if (isConfigured)
                return;
            isConfigured = true;

            // Register services
            builder.RegisterType<EventBus>().As<IEventBus>().SingleInstance();
            builder.RegisterType<PathService>().As<IPathService>().SingleInstance();
        }
    }
}
