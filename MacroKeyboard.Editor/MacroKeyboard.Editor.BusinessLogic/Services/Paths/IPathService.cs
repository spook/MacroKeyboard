﻿namespace MacroKeyboard.Editor.BusinessLogic.Services.Paths
{
    public interface IPathService
    {
        string AppDataPath { get; }
    }
}