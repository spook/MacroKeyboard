﻿using Spooksoft.Wpf.Controls.Panels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MacroKeyboard.Editor.Controls.Editors
{
    /// <summary>
    /// Logika interakcji dla klasy KeyPressActionEditor.xaml
    /// </summary>
    public partial class KeyPressActionEditor : EditorPanel
    {
        public KeyPressActionEditor()
        {
            InitializeComponent();
        }
    }
}
