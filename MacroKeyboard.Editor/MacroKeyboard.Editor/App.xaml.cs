﻿using MacroKeyboard.Editor.Dependencies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MacroKeyboard.Editor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Container.BuildContainer(MacroKeyboard.Editor.Dependencies.Configuration.Configure);
        }
    }
}
