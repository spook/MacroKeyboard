﻿using Autofac;
using MacroKeyboard.Editor.BusinessLogic.Services.Dialogs;
using MacroKeyboard.Editor.BusinessLogic.Services.Messaging;
using MacroKeyboard.Editor.Services.DialogService;
using MacroKeyboard.Editor.Services.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Editor.Dependencies
{
    public static class Configuration
    {
        private static bool configured = false;

        public static void Configure(ContainerBuilder builder)
        {
            if (configured)
                return;

            builder.RegisterType<MessagingService>().As<IMessagingService>().SingleInstance();
            builder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();

            MacroKeyboard.Editor.BusinessLogic.Dependencies.Configuration.Configure(builder);

            configured = true;
        }
    }
}
