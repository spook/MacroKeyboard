#pragma once

#include <vector>
#include <codecvt>
#include <tlhelp32.h>
#include <UIAutomationClient.h>
#include <algorithm>

/// <summary>
/// Contains information about single process and associated windows
/// </summary>
struct ProcessWindowsInfo
{
	unsigned long process_id;
	std::vector<HWND> window_handles;
};

class WindowHandler
{
private:
	std::string process;
	std::string executable;
	IUIAutomation* uiAutomation;

	bool equals_case_insensitive(const wchar_t* a, const wchar_t* b);
	static bool is_main_window(HWND handle);
	static BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam);
	std::vector<ProcessWindowsInfo> find_windows(std::vector<DWORD> process_ids);
	std::vector<DWORD> find_process_ids(const std::wstring& processName);
	bool bring_to_front(HWND hWnd);
	void start_process(const std::string& processPath);

public:

	WindowHandler();
	virtual ~WindowHandler();
	bool activate_main_window(const std::wstring& processName);
};
