// *** Configuration start ***

// Define to send screens via serial port
// Undefine to send to e-paper display
#undef DISPLAY_SCREEN_SERIAL

// *** Configuration end ***

#include <SPI.h>
#include <SD.h>
#include <Keyboard.h>
#include <Mouse.h>
#include <Encoder.h>
#include "src\ePaper\epd2in9_V2.h"
#include "src\Display\sansSerif7.h"

#ifdef DISPLAY_SCREEN_SERIAL
  #include "src\Display\serialDisplay.h"
#else
  #include "src\Display\display.h"
#endif

#include "src\Definition\Definition.h"

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 296

#define ROTATE_0 0
#define ROTATE_90 1
#define ROTATE_180 2
#define ROTATE_270 3

#define BUTTON_COUNT 12

#define ENCODER_MODE_ARROWS 0
#define ENCODER_MODE_MOUSE 1
#define ENCODER_MODE_SCROLL 2
#define ENCODER_MODE_VOLUME 3
#define ENCODER_MODE_CTRL_ARROWS 4

#define MAX_ENCODER_MODE 4

#define ACTION_KEYPRESS 0
#define ACTION_MODIFIER_DOWN 1
#define ACTION_MODIFIER_UP 2
#define ACTION_SWITCH_SCREEN 3
#define ACTION_TOGGLE_ENCODER_MODE 4
#define ACTION_SLEEP 5
#define ACTION_SWITCH_ENCODER_MODE 6
#define ACTION_EXECUTE 7
#define ACTION_ACTIVATE 8

#define BUTTON_ACTION_THRESHOLD_MS 500

unsigned char image[(SCREEN_WIDTH / 8) * SCREEN_HEIGHT];

const int buttonPins[] = { 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 };
const int encoderOutAPin = 4;
const int encoderOutBPin = 3;
const int encoderSwitchPin = 2;

#ifdef DISPLAY_SCREEN_SERIAL
SerialDisplay display;
#else
Display display;
#endif

Encoder encoder(encoderOutAPin, encoderOutBPin);
Definition * definition;
const Screen * currentScreen;
int encoderMode = 0;
int encoderSubmode = 0;

void DieWithError(const char * message)
{
  display.clear();
  display.drawString(message, 0, 0, &sansSerif7);
  display.displayBuffer(true);
  
  #ifndef DISPLAY_SCREEN_SERIAL
  display.forceSleep();
  #endif

  // Enter endless loop to prevent damage
  while (true)
  {
    Serial.print("LOG");
    Serial.print("Critical error: ");
    Serial.print(message);
    Serial.write(0);

    delay(10000);
  }
}

void executeActions(const unsigned char * actionData, const unsigned short actionBytes)
{
  unsigned short modifier = 0;
  Keyboard.set_modifier(modifier);

  int i = 0;
  while (i < actionBytes)
  {
    unsigned char action = actionData[i];
    unsigned char d1 = actionData[i + 1];
    unsigned char d2 = actionData[i + 2];
    unsigned short code = ((short)d2 << 8) + (short)d1;

    if (action == ACTION_KEYPRESS) 
    {
      Keyboard.press(code);
      delay(25);
      Keyboard.release(code);
    }
    else if (action == ACTION_MODIFIER_DOWN)
    {
      Keyboard.press(code);
      delay(25);
    }
    else if (action == ACTION_MODIFIER_UP)
    {      
      Keyboard.release(code);
      delay(25);
    }
    else if (action == ACTION_SWITCH_SCREEN)
    {
      // Switch to screen

      currentScreen = definition->getScreenById(code);
      if (currentScreen == nullptr)
        currentScreen = definition->getScreenById(0);

      currentScreen->render(reinterpret_cast<BaseDisplay *>(&display));
    }
    else if (action == ACTION_TOGGLE_ENCODER_MODE)
    {
      // Toggle encoder mode
      encoderMode++;
      if (encoderMode > MAX_ENCODER_MODE)
        encoderMode = 0;

      encoderSubmode = 0;
    }
    else if (action == ACTION_SLEEP)
    {
      // Clear screen
      display.clear();
      display.displayBuffer(true);

      // Wait for a keypress
      bool pressed = false;
      while (!pressed)
      {
        for (int i = 0; i < BUTTON_COUNT; i++)
          if (digitalRead(buttonPins[i]) == LOW)
            {
              // Wait for release
              while (digitalRead(buttonPins[i]) == LOW) ;
              // Filter out noise
              delay(50);

              pressed = true;
              break;
            }
      }
      
      // Display last screen again
      currentScreen->render(reinterpret_cast<BaseDisplay *>(&display));
    }
    else if (action == ACTION_SWITCH_ENCODER_MODE)
    {
      unsigned char mode = d1;
      
      if (mode >= 0 && mode <= MAX_ENCODER_MODE)
      {
        encoderMode = mode;
        encoderSubmode = 0;
      }
    }
    else if (action == ACTION_EXECUTE || action == ACTION_ACTIVATE)
    {
      unsigned short * res = definition->getResource(code);

      if (res != nullptr)
      {
        if (action == ACTION_EXECUTE)
          Serial.print("EXE");
        else if (action == ACTION_ACTIVATE)
          Serial.print("ACT");

        do
        {
          Serial.write((unsigned char)((*res) & 0xff));
          Serial.write((unsigned char)(((*res) & 0xff00) >> 8));
          res++;
        }
        while (*res != 0);
      
        Serial.write(0);
        Serial.write(0);
      }
    }

    i += ACTION_BYTES;
  }

  // For safety, releasing all keys
  Keyboard.releaseAll();
}

void processButtonPress(int buttonPressed)
{
  const BaseItem * baseItem = currentScreen->getItem(buttonPressed);
  if (baseItem != nullptr && baseItem->getType() == 0)
  {
      const Item * item = (const Item *)(baseItem);
      if (item != nullptr)
      {
          const unsigned char * actions = item->getActionData();
          unsigned char actionBytes = item->getActionBytes();

          // Execute actions
          executeActions(actions, actionBytes);
      }
  }
}

void processEncoderStep(int encoderDir)
{
  switch (encoderMode)
  {
    case ENCODER_MODE_ARROWS:
    {
      if (encoderSubmode == 0)
      {
        if (encoderDir < 0)
        {
          Keyboard.press(KEY_UP_ARROW);
          Keyboard.release(KEY_UP_ARROW);
        }
        else
        {
          Keyboard.press(KEY_DOWN_ARROW);
          Keyboard.release(KEY_DOWN_ARROW);
        }
      }
      else
      {
        if (encoderDir < 0)
        {
          Keyboard.press(KEY_LEFT_ARROW);
          Keyboard.release(KEY_LEFT_ARROW);
        }
        else
        {
          Keyboard.press(KEY_RIGHT_ARROW);
          Keyboard.release(KEY_RIGHT_ARROW);
        }
      }
      
      break;
    }
    case ENCODER_MODE_MOUSE:
    {
      if (encoderSubmode == 0)
      {
        Mouse.move(0, encoderDir);
      }
      else
      {
        Mouse.move(encoderDir, 0);
      }

      break;
    }
    case ENCODER_MODE_SCROLL:
    {
      if (encoderSubmode == 0)
      {
        Mouse.scroll(-encoderDir, 0);
      }
      else
      {
        Mouse.scroll(0, encoderDir);
      }

      break;
    }
    case ENCODER_MODE_VOLUME:
    {
      if (encoderDir < 0)
      {
        Keyboard.press(KEY_MEDIA_VOLUME_DEC);
        Keyboard.release(KEY_MEDIA_VOLUME_DEC);
      }
      else
      {
        Keyboard.press(KEY_MEDIA_VOLUME_INC);
        Keyboard.release(KEY_MEDIA_VOLUME_INC);
      }
      
      break;
    }
    case ENCODER_MODE_CTRL_ARROWS:
    {
      if (encoderSubmode == 0)
      {
        if (encoderDir < 0)
        {
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_UP_ARROW);
          Keyboard.release(KEY_UP_ARROW);
          Keyboard.release(KEY_LEFT_CTRL);
        }
        else
        {
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_DOWN_ARROW);
          Keyboard.release(KEY_DOWN_ARROW);
          Keyboard.release(KEY_LEFT_CTRL);
        }
      }
      else
      {
        if (encoderDir < 0)
        {
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_LEFT_ARROW);
          Keyboard.release(KEY_LEFT_ARROW);
          Keyboard.release(KEY_LEFT_CTRL);
        }
        else
        {
          Keyboard.press(KEY_LEFT_CTRL);
          Keyboard.press(KEY_RIGHT_ARROW);
          Keyboard.release(KEY_RIGHT_ARROW);
          Keyboard.release(KEY_LEFT_CTRL);
        }
      }
      
      break;
    }
  }
}

void processEncoderSwitch()
{
  encoderSubmode = (encoderSubmode + 1) % 2;
}

bool checkButtons()
{
  int buttonPressed = -1;
  for (int i = 0; i < BUTTON_COUNT; i++)
    if (digitalRead(buttonPins[i]) == LOW)
    {
      buttonPressed = i;
      break;
    }

  if (buttonPressed != -1) 
  {
    // Filter out noise
    delay(50);

    elapsedMillis timePressed;
    bool showScreen = false;

    // Wait for button release to avoid multiple calls
    while (digitalRead(buttonPins[buttonPressed]) == LOW)
    {
      if (!showScreen && timePressed >= BUTTON_ACTION_THRESHOLD_MS)
      {
        currentScreen->render(reinterpret_cast<BaseDisplay *>(&display));
        showScreen = true;  
      }
    }

    if (!showScreen)
    {
      processButtonPress(buttonPressed);
    }

    // Filter out noise
    delay(50);

    return true;
  }

  return false;
}

bool checkEncoderRotation()
{
    int encoderState = encoder.read();
  if (encoderState != 0)
  {
    if (encoderState < 0)
      encoderState = -1;
    else
      encoderState = 1;

    processEncoderStep(encoderState);

    encoder.write(0);
    return true;
  }

  return false;
}

bool checkEncoderButton()
{
    int encoderButton = digitalRead(encoderSwitchPin);
  if (encoderButton == LOW)
  {
    processEncoderSwitch();

    // Wait for encoder button release to avoid multiple calls
    while (digitalRead(encoderSwitchPin) == LOW) ;
    // Filter out noise
    delay(50);

    return true;
  }

  return false;
}

void setup() 
{
    Serial.begin(115200);
    
    #ifndef DISPLAY_SCREEN_SERIAL
    // When used with screen, try to connect to serial. 
    // If not possible, start working without it
    
    int serialAttempts = 10;
    while (!Serial && serialAttempts > 0) {
      delay(100);
      serialAttempts--;
    }
    
    #else
    
    // When used without screen, wait until serial port connects
    while (!Serial) ;
    
    #endif

    // *** Initialize display ***

    #ifdef DISPLAY_SCREEN_SERIAL
    display.setRotation(ROTATE_0);
    #else
    display.setRotation(ROTATE_270);
    #endif

    // *** Initialize SD card ***

    if (!SD.begin(BUILTIN_SDCARD)) 
    {
      DieWithError("SD Card reader not initialized!");
    }

    // *** Configure pins ***

    for (int i = 0; i < BUTTON_COUNT; i++)
    {
        pinMode(buttonPins[i], INPUT_PULLUP);
    }

    pinMode(encoderSwitchPin, INPUT_PULLUP);

    // *** Load definition ***

    if (!SD.exists("Keyboard.def"))
    {
      DieWithError("No Keyboard.def file in SD card root!");
    }

    File defFile = SD.open("Keyboard.def", FILE_READ);
    definition = new Definition(&defFile);
    defFile.close();

    // *** Load root screen ***

    currentScreen = definition->getScreenById(0);
    if (currentScreen == nullptr)
      DieWithError("No root screen!");

    currentScreen->render(reinterpret_cast<BaseDisplay *>(&display));

    // display.clear(); display.displayBuffer();
}

void loop() {

  // Allow display to check, if there is a need to sleep ePaper display
  #ifndef DISPLAY_SCREEN_SERIAL
  display.tick();
  #endif

  checkButtons();
  checkEncoderRotation();
  checkEncoderButton();
}
