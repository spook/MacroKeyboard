#ifndef __ERROR_HANDLING_H__
#define __ERROR_HANDLING_H__

void DieWithError(const char * message);

#endif