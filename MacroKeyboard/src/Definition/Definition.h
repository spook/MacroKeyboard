#ifndef __DEFINITION_H__
#define __DEFINITION_H__

#include <Arduino.h>
#include "..\Display\display.h"

#define ICON_WIDTH 32
#define ICON_HEIGHT 32
#define ACTION_BYTES 3

class BaseItem 
{
protected:
    BaseItem();

public:
    virtual int getType() const = 0;
    static BaseItem * FromStream(Stream * stream);
    virtual void render(BaseDisplay * display, int x, int y) = 0;
};

class EmptyItem : public BaseItem
{
public:
    EmptyItem(Stream * stream);
    void render(BaseDisplay * display, int x, int y);
    int getType() const;
};

class Item : public BaseItem
{
private:
    // In version 1, each action occupies exactly 3 bytes.
    unsigned short actionBytes;
    unsigned char * actionData;    
    unsigned char * icon;
    char * title;

public:
    Item(Stream * stream);
    void render(BaseDisplay * display, int x, int y);
    int getType() const;
    const unsigned char * getActionData() const;
    const unsigned short getActionBytes() const;
};

class Screen 
{
private:
    unsigned short id;
    unsigned short itemCount;
    BaseItem ** items;

public:
    Screen(Stream * stream);

    unsigned short getId() const;
    void render(BaseDisplay * display) const;
    const BaseItem * getItem(unsigned short index) const;
};

class Definition
{
private:
    unsigned short screenCount;
    Screen ** screens;
    std::vector<unsigned short *> resources;

public:
    Definition(Stream * stream);

    unsigned short getScreenCount();
    const Screen * getScreenById(unsigned short id) const;
    unsigned short getResourceCount();
    unsigned short * getResource(unsigned short id);
};

#endif