#include "Definition.h"
#include "..\ErrorHandling.h"
#include "..\Display\sansSerif7.h"

// BaseItem -------------------------------------------------------------------

BaseItem::BaseItem()
{

}

BaseItem * BaseItem::FromStream(Stream * stream)
{
    // Serial.println("Loading BaseItem from stream...");

    unsigned char type;
    int read = stream->readBytes(&type, 1);
    if (read != 1)
        DieWithError("Unexpected end of file (item type)");

    // Serial.print("  Type: ");
    // Serial.println(type);

    if (type == 0)
        return new Item(stream);
    else if (type == 1)
        return new EmptyItem(stream);
    else
        DieWithError("Invalid item type!");

    return nullptr;
}

// EmptyItem ------------------------------------------------------------------

EmptyItem::EmptyItem(Stream * stream)
{
    // Serial.println("Loading empty item from stream...");

    // Dummy item, serving as a placeholder.
    // Nothing to read here.
}

void EmptyItem::render(BaseDisplay * display, int x, int y)
{
    // Dummy item, nothing to render.
}

int EmptyItem::getType() const
{
    return 1;
}

// Item -----------------------------------------------------------------------

Item::Item(Stream * stream)
{
    // Serial.println("Loading item from stream...");

    unsigned char titleLength;
    int read = stream->readBytes(&titleLength, 1);
    if (read != 1)
        DieWithError("Unexpected end of file (item title length)");

    // Serial.print("  Title length: ");
    // Serial.println(titleLength);

    this->title = new char[titleLength + 1];
    memset(this->title, 0, titleLength + 1);

    read = stream->readBytes(this->title, titleLength);
    if (read != titleLength)
        DieWithError("Unexpected end of file (item title)");

    // Serial.print("  Title: ");
    // Serial.println(this->title);

    const int iconByteSize = (ICON_WIDTH / 8) * ICON_HEIGHT;
    this->icon = new unsigned char[iconByteSize];
    read = stream->readBytes(this->icon, iconByteSize);
    if (read != iconByteSize)
        DieWithError("Unexpected end of file (item icon)");

    // Serial.println("  Icon: (data)");

    read = stream->readBytes(reinterpret_cast<unsigned char *>(&(this->actionBytes)), 2);
    if (read != 2)
        DieWithError("Unexpected end of file (item action byte count)");

    // Serial.print("  Action bytes: ");
    // Serial.println(actionBytes);

    actionData = new unsigned char[this->actionBytes];
    read = stream->readBytes(this->actionData, this->actionBytes);
    if (read != this->actionBytes)
        DieWithError("Unexpected end of file (item actions)");

    // Serial.println("  Action data: (data)");
}

void Item::render(BaseDisplay * display, int x, int y)
{
    display->drawImage(this->icon, x - ICON_WIDTH / 2, y, ICON_WIDTH, ICON_HEIGHT);

    int w, h;
    display->measureString(this->title, &sansSerif7, w, h);

    int textX = x - (w / 2);
    int textY = y + ICON_HEIGHT + 2;

    display->drawString(this->title, textX, textY, &sansSerif7);
}

int Item::getType() const
{
    return 0;
}

const unsigned char * Item::getActionData() const
{
    return this->actionData;
}

const unsigned short Item::getActionBytes() const
{
    return this->actionBytes;
}

// Screen ---------------------------------------------------------------------

Screen::Screen(Stream * stream)
{
    // Serial.println("Loading screen from stream...");

    int read = stream->readBytes(reinterpret_cast<unsigned char *>(&(this->id)), 2);
    if (read != 2)
        DieWithError("Unexpected end of file (screen id)");

    // Serial.print("  Id: ");
    // Serial.println(this->id);

    read = stream->readBytes(reinterpret_cast<unsigned char *>(&(this->itemCount)), 2);
    if (read != 2)
        DieWithError("Unexpected end of file (screen item count)");

    // Serial.print("  Item count: ");
    // Serial.println(this->itemCount);

    this->items = new BaseItem * [this->itemCount];

    for (int i = 0; i < this->itemCount; i++)
    {
        this->items[i] = BaseItem::FromStream(stream);
    }
}

unsigned short Screen::getId() const 
{
    return this->id;
}

void Screen::render(BaseDisplay * display) const 
{
    display->clear();

    for (unsigned int i = 0; i < this->itemCount; i++) 
    {
        int x = (1 + 2 * (i % 4)) * (296 / 8);
        int y = (i / 4) * (128 / 3);
        this->items[i]->render(display, x, y);
    }

    display->displayBuffer(false);
}

const BaseItem * Screen::getItem(unsigned short index) const
{
    if (index >= this->itemCount)
        return nullptr;

    return items[index];
}

// Definition -----------------------------------------------------------------

Definition::Definition(Stream * stream)
{
    // Serial.println("Loading definition from stream...");

    int read = stream->readBytes(reinterpret_cast<unsigned char *>(&(this->screenCount)), 2);
    if (read != 2)
        DieWithError("Unexpected end of file (definition screen count)");

    // Serial.print("  Screen count: ");
    // Serial.println(this->screenCount);

    this->screens = new Screen * [this->screenCount];

    for (unsigned int i = 0; i < this->screenCount; i++)
    {
        screens[i] = new Screen(stream);
    }

    // Loading resources

    unsigned short resourceCount;
    read = stream->readBytes(reinterpret_cast<unsigned char *>(&resourceCount), 2);
    if (read != 2)
        DieWithError("Unexpected end of file (resource count)");

    unsigned short longestResource;
    read = stream->readBytes(reinterpret_cast<unsigned char *>(&longestResource), 2);
    if (read != 2)
        DieWithError("Unexpected end of file (longest resource)");

    if (resourceCount > 0 && longestResource > 0)
    {
        unsigned short * buffer = new unsigned short[longestResource];

        for (unsigned int i = 0; i < resourceCount; i++)
        {
            int pos = 0;
            unsigned short ch;

            do
            {
                read = stream->readBytes(reinterpret_cast<unsigned char *>(&ch), 2);
                if (read != 2)
                    DieWithError("Unexpected end of file (reading resources)");

                if (pos == longestResource)
                    DieWithError("Resource longer than declared longest one!");

                buffer[pos++] = ch;
            } 
            while (ch != 0);

            unsigned short * res = new unsigned short[pos];
            memcpy(res, buffer, pos * 2);

            this->resources.push_back(res);
        }
    
        delete[] buffer;
    }
}

unsigned short Definition::getScreenCount()
{
    return this->screenCount;
}

const Screen * Definition::getScreenById(unsigned short id) const
{
    for (int i = 0; i < screenCount; i++)
    {
        if (screens[i]->getId() == id)
            return screens[i];
    }

    return nullptr;
}

unsigned short Definition::getResourceCount()
{
    return resources.size();
}

unsigned short * Definition::getResource(unsigned short id)
{
    if (id >= 0 || id < resources.size())
        return resources[id];
    else
        return nullptr;
}
