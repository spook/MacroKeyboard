#ifndef __FONT_H__
#define __FONT_H__

#include <vector>

struct Display_font_glyph
{
    int offset;
    int originX;
    int originY;
    unsigned int width;
    unsigned int height;
    unsigned int delta;
};

struct Display_font 
{
    const unsigned char * data;
    const Display_font_glyph * glyphs;
    unsigned char rangeStart;
    unsigned char rangeEnd;
};

#endif