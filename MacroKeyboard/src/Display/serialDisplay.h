#ifndef __SERIALDISPLAY_H__
#define __SERIALDISPLAY_H__

#include <Arduino.h>
#include "baseDisplay.h"
#include "font.h"

class SerialDisplay : public BaseDisplay {
public:
    SerialDisplay();

    void displayBuffer(bool forceFull);
};

#endif