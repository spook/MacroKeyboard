#ifndef __sansSerif7_h__
#define __sansSerif7_h__

#include "font.h"

#ifdef __cplusplus
extern "C" {
#endif

extern const Display_font sansSerif7;

#ifdef __cplusplus
} // extern "C"
#endif

#endif
