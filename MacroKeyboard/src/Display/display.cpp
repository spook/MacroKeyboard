#include "display.h"

void Display::init()
{    
    epd.Init();
    isSleeping = false;
    partialRefreshes = 0;
}

void Display::sleep()
{
    epd.Sleep();
    isSleeping = true;
    partialRefreshes = 0;
}

void Display::displayBufferInternal()
{
    epd.SetFrameMemory(this->buffer, 0, 0, this->width, this->height);
    epd.DisplayFrame();
}

void Display::displayBufferPartialInternal()
{
    if (partialRefreshes == 0)
    {
        epd.SetFrameMemory_Base(this->buffer);
        epd.DisplayFrame();
    }

    epd.SetFrameMemory_Partial(this->buffer, 0, 0, this->width, this->height);
    epd.DisplayFrame_Partial();
}

Display::Display()
    : BaseDisplay(128, 296)
{
    this->isSleeping = true;
    this->millisSinceLastDisplay = 0;
    this->partialRefreshes = 0;
}

void Display::tick()
{
    if (isSleeping)
        return;

    if (millisSinceLastDisplay > TIME_TO_SLEEP)
        this->sleep();
}

void Display::forceSleep()
{
    this->sleep();
}

void Display::displayBuffer(bool forceFull)
{
    if (forceFull)
        this->sleep();

    if (isSleeping)
    {
        this->init();
        this->displayBufferInternal();
    }
    else
    {
        this->displayBufferPartialInternal();
        partialRefreshes++;
    }
    
    millisSinceLastDisplay = 0;

    if (partialRefreshes == MAX_PARTIAL_REFRESHES)
    {
        sleep();
    }
}
