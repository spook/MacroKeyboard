#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "baseDisplay.h"
#include "..\ePaper\epd2in9_V2.h"
#include "font.h"

class Display : public BaseDisplay {
private:
    const int MAX_PARTIAL_REFRESHES = 5;
    const long int TIME_TO_SLEEP = 10000; // 10 seconds

    Epd epd;
    elapsedMillis millisSinceLastDisplay;
    bool isSleeping;
    int partialRefreshes;

    void init();
    void sleep();
    void displayBufferInternal();
    void displayBufferPartialInternal();

public:
    Display();

    void tick();
    void forceSleep();

    void displayBuffer(bool forceFull) override;
};

#endif