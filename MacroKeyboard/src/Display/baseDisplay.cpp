#include "baseDisplay.h"
#include "font.h"

void BaseDisplay::drawAbsolutePixel(int x, int y, bool white)
{
    if (white)
    {
        this->buffer[(y * this->width / 8) + (x / 8)] |= (0x80 >> (x % 8));
    }
    else
    {
        this->buffer[(y * this->width / 8) + (x / 8)] &= ~(0x80 >> (x % 8));
    }
}

BaseDisplay::BaseDisplay(int width, int height)
    : width(width), height(height), bufferSize((width / 8) * height)
{
    buffer = new unsigned char[this->bufferSize];
    memset(this->buffer, 0xFF, this->bufferSize);
    rotate = ROTATE_0;
}

void BaseDisplay::clear()
{
    memset(this->buffer, 0xFF, this->bufferSize);
}

void BaseDisplay::setRotation(int newRotate)
{
    if (newRotate >= ROTATE_0 && newRotate <= ROTATE_270)
    {
        this->rotate = newRotate;
    }
}

void BaseDisplay::drawPixel(int x, int y, bool white)
{
    if (this->rotate == ROTATE_0) 
    {
        if (x < 0 || x >= this->width || y < 0 || y >= this->height) 
            return;

        drawAbsolutePixel(x, y, white);
    } 
    else if (this->rotate == ROTATE_90) 
    {
        if(x < 0 || x >= this->height || y < 0 || y >= this->width) 
          return;
        
        drawAbsolutePixel(this->width - 1 - y, x, white);
    } 
    else if (this->rotate == ROTATE_180) 
    {
        if (x < 0 || x >= this->width || y < 0 || y >= this->height)
          return;
        
        drawAbsolutePixel(this->width - 1 - x, this->height - 1 - y, white);
    } 
    else if (this->rotate == ROTATE_270) 
    {
        if (x < 0 || x >= this->height || y < 0 || y >= this->width)
          return;
       
        drawAbsolutePixel(y, this->height - 1 - x, white);
    }
}

void BaseDisplay::drawImage(const unsigned char * source, int x, int y, int w, int h)
{
    int stride = w / 8 + (w % 8 > 0 ? 1 : 0);

    for (int y1 = 0; y1 < h; y1++)
    {
        for (int x1 = 0; x1 < w; x1++)
        {
            bool white = (source[(y1)*stride + (x1) / 8] & (0x80 >> (x1 % 8))) != 0;
            drawPixel(x + x1, y + y1, white);
        }
    }
}

void BaseDisplay::drawString(const char * text, int x, int y, const Display_font * font)
{
    const char * ch = text;

    while (*ch != 0)
    {
        if (*ch >= font->rangeStart && *ch <= font->rangeEnd)
        {
            const Display_font_glyph * glyph = font->glyphs + (*ch - font->rangeStart);

            int drawX = x + glyph->originX;
            int drawY = y + glyph->originY;
            const unsigned char * glyphImage = font->data + glyph->offset;

            drawImage(glyphImage, drawX, drawY, glyph->width, glyph->height);

            x += glyph->delta;
        }

        ch++;
    }
}

void BaseDisplay::measureString(const char * text, const Display_font * font, int & w, int & h)
{
    w = 0;
    h = 0;

    const char * ch = text;
    while (*ch != 0)
    {
        if (*ch >= font->rangeStart && *ch <= font->rangeEnd)
        {
            const Display_font_glyph * glyph = font->glyphs + (*ch - font->rangeStart);

            w += glyph->delta;

            int gh = glyph->height - glyph->originY;
            if (gh > h)
                h = gh;
        }

        ch++;
    }
}