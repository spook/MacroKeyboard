#ifndef __BASEDISPLAY_H__
#define __BASEDISPLAY_H__

#include <Arduino.h>
#include "font.h"

#define ROTATE_0 0
#define ROTATE_90 1
#define ROTATE_180 2
#define ROTATE_270 3

// #define SCREEN_WIDTH 128
// #define SCREEN_HEIGHT 296
// (SCREEN_WIDTH / 8) * SCREEN_HEIGHT
// #define BUFFER_SIZE 4736

class BaseDisplay
{
private:
    int rotate;

protected:
    const int width, height, bufferSize;
    unsigned char * buffer;

    void drawAbsolutePixel(int x, int y, bool white);

public:
    BaseDisplay(int width, int height);

    void clear();
    void drawImage(const unsigned char * source, int x, int y, int w, int h);
    void measureString(const char * text, const Display_font * font, int & w, int & h);
    void drawString(const char * text, int x, int y, const Display_font * font);
    void setRotation(int newRotate);
    void drawPixel(int x, int y, bool white);

    virtual void displayBuffer(bool forceFull) = 0;
};

#endif