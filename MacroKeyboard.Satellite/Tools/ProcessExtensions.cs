﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Tools
{
    public static class ProcessExtensions
    {
        public static ProcessModule TryGetMainModule(this Process process)
        {
            try
            {
                return process.MainModule;
            }
            catch
            {
                return null;
            }
        }
    }
}
