﻿using MacroKeyboard.Satellite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.ViewModels
{
    public class ShowLogEntryEventArgs : EventArgs
    {
        public ShowLogEntryEventArgs(DateTime time, string source, string message)
        {
            Source = source;
            Message = message;
        }

        public DateTime Time { get; }
        public string Source { get; }
        public string Message { get; }
    }

    public delegate void ShowLogEntryEventHandler(object sender, ShowLogEntryEventArgs e);

    public class LogFormViewModel
    {
        private readonly LogService logService;

        private void HandleLogReceived(object sender, LogReceivedEventArgs e)
        {
            ShowLogEntry?.Invoke(this, new ShowLogEntryEventArgs(e.TimeReceived, e.Source, e.Message));
        }

        public LogFormViewModel(LogService logService)
        {
            this.logService = logService;
            logService.LogReceived += HandleLogReceived;
        }

        public void NotifyClosing()
        {
            logService.LogReceived -= HandleLogReceived;
        }

        public event ShowLogEntryEventHandler ShowLogEntry;       
    }
}
