﻿using MacroKeyboard.Satellite.Models.Commands;
using MacroKeyboard.Satellite.Services;
using MacroKeyboard.Satellite.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace MacroKeyboard.Satellite.ViewModels
{
    public class ShowScreenEventArgs : EventArgs
    {
        public ShowScreenEventArgs(Bitmap screen)
        {
            Screen = screen;
        }

        public Bitmap Screen { get; }
    }

    public delegate void ShowScreenEventHandler(object sender, ShowScreenEventArgs e);

    public class PreviewFormViewModel
    {
        private const string DeviceLogSource = "Device";

        private readonly CommandReceivierService commandReceivier;
        private readonly SerialPortService serialPortService;
        private readonly LogService logService;
        private readonly WindowHandlerService windowHandlerService;

        private void DoExecuteCommand(string command)
        {
            int i = 0;
            bool inQuotes = false;

            while (i < command.Length && (inQuotes || command[i] != ' '))
            {
                if (command[i] == '"')
                    inQuotes = !inQuotes;

                i++;
            }

            var startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.UseShellExecute = true;          

            if (i < command.Length)
            {
                string fileName = command[..i];
                string args = command[(i + 1)..];

                startInfo.FileName = fileName;
                startInfo.Arguments = args;
                
                Process.Start(startInfo);
            }
            else
            {
                startInfo.FileName = command;

                Process.Start(startInfo);
            }
        }

        private void HandleCommandReceived(object sender, CommandEventArgs e)
        {
            if (e.Command is ScreenCommand screenCommand)
            {
                ShowScreen?.Invoke(this, new ShowScreenEventArgs(screenCommand.Screen));
            }
            else if (e.Command is LogCommand logCommand)
            {
                logService.AddLog(DeviceLogSource, $"{logCommand.Log}");
            }
            else if (e.Command is ExecuteCommand executeCommand)
            {
                DoExecuteCommand(executeCommand.Command);               
            }
            else if (e.Command is ActivateCommand bringToFrontCommand)
            {
                windowHandlerService.ActivateMainWindow(bringToFrontCommand.Binary);
            }
            else
                throw new InvalidOperationException("Unsupported screen command!");
        }

        internal void OpenPort(string port)
        {
            commandReceivier.Reset();
            serialPortService.OpenPort(port);
        }

        public PreviewFormViewModel(CommandReceivierService commandReceivier,
            SerialPortService serialPortService,
            LogService logService, 
            WindowHandlerService windowHandlerService)
        {
            this.commandReceivier = commandReceivier;
            this.serialPortService = serialPortService;
            this.logService = logService;
            this.windowHandlerService = windowHandlerService;

            commandReceivier.CommandReceived += HandleCommandReceived;           
        }

        public void Exit()
        {
            serialPortService.ClosePort();

            ExitApplication?.Invoke(this, EventArgs.Empty);
        }

        public event ShowScreenEventHandler? ShowScreen;

        public event EventHandler? ExitApplication;
    }
}
