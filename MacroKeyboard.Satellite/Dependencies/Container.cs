﻿using MacroKeyboard.Satellite.Services;
using MacroKeyboard.Satellite.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Dependencies
{
    public static class Container
    {
        private static IHost? host;
        private static IServiceScope? globalScope;

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<SerialPortService>();
            services.AddSingleton<CommandReceivierService>();
            services.AddSingleton<PathService>();
            services.AddSingleton<ConfigurationService>();
            services.AddSingleton<LogService>();
            services.AddSingleton<WindowHandlerService>();

            services.AddTransient<PreviewFormViewModel>();
            services.AddTransient<LogFormViewModel>();
        }

        public static void Configure(IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices(ConfigureServices);
        }

        public static void Setup(IHost host)
        {
            Container.host = host;
            globalScope = host.Services.CreateScope();
        }

        public static T? Resolve<T>()
        {
            if (globalScope == null)
                throw new InvalidOperationException("Container is not configured!");

            return globalScope.ServiceProvider.GetService<T>();
        }
    }
}
