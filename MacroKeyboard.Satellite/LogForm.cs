﻿using MacroKeyboard.Satellite.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacroKeyboard.Satellite
{
    public partial class LogForm : Form
    {
        private readonly LogFormViewModel? viewModel;

        private string PadRight(string str, int length)
        {
            StringBuilder result = new StringBuilder();
            result.Append(str);
            while (result.Length < length)
                result.Append(' ');

            return result.ToString();
        }

        private void HandleShowLogEntry(object sender, ShowLogEntryEventArgs e)
        {
            this.BeginInvoke(() => { 
                tbLog.Text += $"{PadRight(e.Time.ToString("HH:mm:ss.ff"), 14)} | {PadRight(e.Source, 24)} | {e.Message}\r\n";
                tbLog.SelectionStart = tbLog.Text.Length;
                tbLog.ScrollToCaret();
            });
        }

        public LogForm()
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Resolve<LogFormViewModel>();
            viewModel.ShowLogEntry += HandleShowLogEntry;
        }

        private void LogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            viewModel.NotifyClosing();
        }
    }
}
