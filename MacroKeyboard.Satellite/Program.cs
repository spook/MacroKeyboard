using MacroKeyboard.Satellite.Dependencies;
using MacroKeyboard.Satellite.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MacroKeyboard.Satellite
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var builder = Host.CreateDefaultBuilder();
            Container.Configure(builder);

            using IHost host = builder.Build();
            Container.Setup(host);

            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.

            ApplicationConfiguration.Initialize();

            var previewForm = new PreviewForm();
            var temp = previewForm.Handle;

            Application.Run();
        }
    }
}