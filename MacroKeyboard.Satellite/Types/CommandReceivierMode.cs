﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Types
{
    public enum CommandReceivierMode
    {
        ReceivingCommand,
        ReceivingData
    }
}
