using MacroKeyboard.Satellite.Services;
using MacroKeyboard.Satellite.Tools;
using MacroKeyboard.Satellite.ViewModels;
using System.IO.Ports;

namespace MacroKeyboard.Satellite
{
    public partial class PreviewForm : Form
    {
        private const int SCREEN_MARGIN = 16;

        private NotifyIcon trayIcon;
        private readonly PreviewFormViewModel? viewModel;
        private readonly SerialPortService serialPortService;
        private bool? autoFade;

        private void ShowAndPosition(bool timerFade)
        {
            var mainScreen = Screen.PrimaryScreen;
            Show();
            this.Location = new Point(mainScreen.WorkingArea.Right - Width - SCREEN_MARGIN, mainScreen.WorkingArea.Bottom - Height - SCREEN_MARGIN);

            fadeTimer.Stop();

            if (autoFade == true && timerFade)
            {
                fadeTimer.Start();
            }
        }

        private void HideAndResetFade()
        {
            Hide();
            autoFade = null;
        }

        private void HandleTrayIconMouseUp(object? sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!Visible)
                {
                    // Position and show window
                    autoFade = false;
                    ShowAndPosition(false);
                }
                else
                {
                    HideAndResetFade();
                }
            }
        }

        private void TrayPortMenuItemClick(object? sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem menu && menu.Tag is string port)
            {
                viewModel.OpenPort(port);                
            }
        }

        private void TrayExitMenuItemClick(object? sender, EventArgs e)
        {
            viewModel.Exit();
        }

        private void TrayLogMenuItemClick(object? sender, EventArgs e)
        {
            var logForm = new LogForm();
            logForm.Show();
        }

        private void HandleTrayIconMouseDown(object? sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip trayMenu = new ContextMenuStrip();

                bool anyPortAdded = false;

                string[] ports = serialPortService.GetAvailablePorts();
                foreach (var port in ports)
                {
                    anyPortAdded = true;

                    var item = new ToolStripMenuItem(port);
                    item.Tag = port;
                    item.Checked = port == serialPortService.Port;
                    item.Click += TrayPortMenuItemClick;
                    trayMenu.Items.Add(item);
                }

                if (anyPortAdded)
                {
                    var separator = new ToolStripSeparator();
                    trayMenu.Items.Add(separator);
                }

                var logItem = new ToolStripMenuItem("Log");
                logItem.Click += TrayLogMenuItemClick;
                trayMenu.Items.Add(logItem);

                var exitItem = new ToolStripMenuItem("Exit");
                exitItem.Click += TrayExitMenuItemClick;
                trayMenu.Items.Add(exitItem);

                trayIcon.ContextMenuStrip = trayMenu;
            }
        }

        private void HandleShowScreenCommand(object sender, ShowScreenEventArgs e)
        {
            this.BeginInvoke(() =>
            {
                pictureBox1.Image = e.Screen;

                if (autoFade == null)
                    autoFade = true;

                ShowAndPosition(true);
            });
        }

        private void HandleExitApplication(object? sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }

        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            HideAndResetFade();
        }

        private void HandleFormClick(object sender, EventArgs e)
        {
            HideAndResetFade();
        }

        protected override CreateParams CreateParams 
        {
            get
            {
                var baseParams = base.CreateParams;
                baseParams.ExStyle |= (Win32.WS_EX_TOPMOST | Win32.WS_EX_NOACTIVATE);
                return baseParams;
            }
        }

        public PreviewForm()
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Resolve<PreviewFormViewModel>();
            viewModel.ShowScreen += HandleShowScreenCommand;
            viewModel.ExitApplication += HandleExitApplication;

            serialPortService = Dependencies.Container.Resolve<SerialPortService>();
            
            trayIcon = new();
            trayIcon.Text = "Macro keyboard satellite";
            trayIcon.MouseDown += HandleTrayIconMouseDown;
            trayIcon.MouseUp += HandleTrayIconMouseUp;
            trayIcon.Icon = this.Icon;
            trayIcon.Visible = true;

            autoFade = null;
        }        
    }
}