﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Services
{
    public class LogReceivedEventArgs : EventArgs
    {
        public LogReceivedEventArgs(string source, string message)
        {
            Source = source;
            Message = message;
        }

        public DateTime TimeReceived { get; } = DateTime.Now;
        public string Source { get; }
        public string Message { get; }
    }

    public delegate void LogReceivedEventHandler(object sender, LogReceivedEventArgs e);

    public class LogService
    {
        public void AddLog(string source, string message) => LogReceived?.Invoke(this, new LogReceivedEventArgs(source, message));

        public event LogReceivedEventHandler LogReceived;
    }
}
