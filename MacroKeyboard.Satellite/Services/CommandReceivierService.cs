﻿using MacroKeyboard.Satellite.Models;
using MacroKeyboard.Satellite.Models.Commands;
using MacroKeyboard.Satellite.Types;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Services
{
    public class CommandEventArgs : EventArgs
    {
        public CommandEventArgs(BaseCommand command)
        {
            Command = command;
        }

        public BaseCommand Command { get; }
    }

    public delegate void CommandReceivedDelegate(object sender, CommandEventArgs e);

    public class CommandReceivierService
    {
        private const string SCREEN_COMMAND = "SCR";
        private const string EXECUTE_COMMAND = "EXE";
        private const string LOG_COMMAND = "LOG";
        private const string ACTIVATE_COMMAND = "ACT";

        private readonly SerialPortService serialPortService;
        private readonly LogService logService;

        private CommandReceivierMode mode;
        
        private readonly MemoryStream buffer;
        private string? command = null;

        private bool IsKnownCommand(string? command)
        {
            return command == SCREEN_COMMAND || command == EXECUTE_COMMAND || command == LOG_COMMAND || command == ACTIVATE_COMMAND;
        }

        private Bitmap DecodeScreenFromBuffer()
        {
            Bitmap bitmap = new Bitmap(296, 128, System.Drawing.Imaging.PixelFormat.Format1bppIndexed);
            buffer.Seek(0, SeekOrigin.Begin);
            byte[] rowBytes = new byte[296 / 8];

            for (int y = 0; y < bitmap.Height; y++)
            {
                var row = bitmap.LockBits(new Rectangle(0, y, 296, 1), ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);
                buffer.Read(rowBytes, 0, rowBytes.Length);
                Marshal.Copy(rowBytes, 0, row.Scan0, rowBytes.Length);
                bitmap.UnlockBits(row);
            }

            return bitmap;
        }

        private bool ReadFixedSize(int expectedSize)
        {
            byte[] byteBuffer = new byte[1024];
            int remainingBytes = (int)(expectedSize - buffer.Length);

            do
            {
                int bytesRead = serialPortService.ReadBytes(byteBuffer, Math.Min(remainingBytes, byteBuffer.Length));
                if (bytesRead == 0)
                {
                    return false;
                }

                buffer.Write(byteBuffer, 0, bytesRead);
                remainingBytes -= bytesRead;    
            }
            while (remainingBytes > 0);

            return true;
        }

        private bool ReadWchars()
        {
            int readByte = 0;
            byte b1 = 0, b2 = 0;

            if (buffer.Length % 2 == 1)
            {
                buffer.Seek(-1, SeekOrigin.End);
                b1 = (byte)buffer.ReadByte();
                readByte = 1;
            }

            bool canRead = true;

            do
            {
                if (readByte == 0)
                {
                    // Reading first byte
                    (canRead, b1) = serialPortService.ReadByte();
                    if (canRead)
                        buffer.WriteByte(b1);

                    readByte = 1;
                }
                else if (readByte == 1)
                {
                    // Reading second byte
                    (canRead, b2) = serialPortService.ReadByte();
                    if (canRead)
                        buffer.WriteByte(b2);

                    readByte = 0;
                }

                if (canRead && readByte == 0 && b1 == 0 && b2 == 0)
                    return true;
            }
            while (canRead);

            return false;
        }

        private bool ReadChars()
        {
            bool canRead = true;

            do
            {
                byte b;
                (canRead, b) = serialPortService.ReadByte();
                if (canRead)
                {
                    buffer.WriteByte(b);

                    if (b == 0)
                        return true;
                }
                
            }
            while (canRead);

            return false;
        }

        private void EmitCommand()
        {
            switch (command)
            {
                case SCREEN_COMMAND:
                    {
                        Bitmap temp = new Bitmap(296, 129, PixelFormat.Format32bppArgb);
                        buffer.Seek(0, SeekOrigin.Begin);
                        for (int y = 0; y < 128; y++)
                            for (int x = 0; x < 296; x++)
                            {
                                int offset = y * (296 / 8) + (x / 8);

                                buffer.Seek(offset, SeekOrigin.Begin);
                                int b = buffer.ReadByte();
                                for (int i = 0; i < 7 - (x % 8); i++)
                                    b /= 2;

                                if (b % 2 == 0)
                                    temp.SetPixel(x, y, Color.FromArgb(255, 0, 0, 0));
                                else
                                    temp.SetPixel(x, y, Color.FromArgb(255, 255, 255, 255));
                            }

                        Bitmap bitmap = DecodeScreenFromBuffer();
                        var command = new ScreenCommand(bitmap);
                        CommandReceived?.Invoke(this, new CommandEventArgs(command));

                        break;
                    }
                case EXECUTE_COMMAND:
                    {
                        buffer.Seek(0, SeekOrigin.Begin);
                        byte[] data = buffer.ToArray();

                        string commandText = Encoding.Unicode.GetString(data, 0, data.Length - 2);

                        var command = new ExecuteCommand(commandText);
                        CommandReceived?.Invoke(this, new CommandEventArgs(command));

                        break;
                    }
                case ACTIVATE_COMMAND:
                    {
                        buffer.Seek(0, SeekOrigin.Begin);
                        byte[] data = buffer.ToArray();

                        string binary = Encoding.Unicode.GetString(data, 0, data.Length - 2);

                        var command = new ActivateCommand(binary);
                        CommandReceived?.Invoke(this, new CommandEventArgs(command));

                        break;
                    }
                case LOG_COMMAND:
                    {
                        buffer.Seek(0, SeekOrigin.Begin);
                        byte[] data = buffer.ToArray();

                        string log = Encoding.ASCII.GetString(data, 0, data.Length - 1);

                        var command = new LogCommand(log);
                        CommandReceived?.Invoke(this, new CommandEventArgs(command));

                        break;
                    }
                default:
                    throw new InvalidOperationException("Unsupported command!");
            }
        }

        private void HandleSerialPortDataReceived(object? sender, EventArgs e)
        {
            bool canReadMoreData = true;

            while (canReadMoreData)
            {
                switch (mode)
                {
                    case CommandReceivierMode.ReceivingCommand:
                        {
                            while (canReadMoreData && buffer.Length < 3)
                            {
                                (canReadMoreData, byte data) = serialPortService.ReadByte();
                                if (canReadMoreData)
                                {
                                    buffer.WriteByte(data);
                                }
                            }

                            if (buffer.Length == 3)
                            {
                                // Decode command
                                buffer.Seek(0, SeekOrigin.Begin);
                                byte[] cmd = new byte[3];
                                buffer.Read(cmd, 0, 3);
                                command = Encoding.ASCII.GetString(cmd);

                                ResetBuffer();

                                if (IsKnownCommand(command))
                                {
                                    mode = CommandReceivierMode.ReceivingData;
                                }
                                else
                                {
                                    // Now we have a problem - malformed communication.
                                    // Clearing all data on input
                                    while (canReadMoreData)
                                    {
                                        (canReadMoreData, _) = serialPortService.ReadByte();
                                    }

                                    // Resetting mode to reading command in hope, that
                                    // eventually a valid command will come

                                    mode = CommandReceivierMode.ReceivingCommand;
                                    command = null;
                                }
                            }

                            break;
                        }
                    case CommandReceivierMode.ReceivingData:
                        {
                            if (command == SCREEN_COMMAND)
                            {
                                if (ReadFixedSize((296 / 8) * 128))
                                {
                                    EmitCommand();
                                    ResetBuffer();
                                    mode = CommandReceivierMode.ReceivingCommand;
                                }
                                else
                                    canReadMoreData = false;
                            }
                            else if (command == EXECUTE_COMMAND || command == ACTIVATE_COMMAND)
                            {
                                if (ReadWchars())
                                {
                                    EmitCommand();
                                    ResetBuffer();
                                    mode = CommandReceivierMode.ReceivingCommand;
                                }
                            }
                            else
                                throw new InvalidOperationException("Unsupported command!");

                            break;
                        }
                }
            }
        }

        private void ResetBuffer()
        {
            buffer.SetLength(0);
        }

        public CommandReceivierService(SerialPortService serialPortService, LogService logService)
        {
            this.serialPortService = serialPortService;
            this.logService = logService;

            buffer = new MemoryStream();
            mode = CommandReceivierMode.ReceivingCommand;

            serialPortService.DataReceived += HandleSerialPortDataReceived;
        }

        public void Reset()
        {
            mode = CommandReceivierMode.ReceivingCommand;
            ResetBuffer();
            command = null;
        }

        public event CommandReceivedDelegate? CommandReceived;
    }
}
