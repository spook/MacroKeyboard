﻿using MacroKeyboard.Satellite.Models.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace MacroKeyboard.Satellite.Services
{
    public class ConfigurationService
    {
        // Private fields -----------------------------------------------------

        private Configuration configuration;
        private readonly PathService pathService;

        // Private methods ----------------------------------------------------

        private string GetConfigPath() => pathService.GetConfigFilePath();

        // Public methods -----------------------------------------------------

        public ConfigurationService(PathService pathService)
        {
            this.pathService = pathService;

            // Defaults
            configuration = new Configuration();

            // Load configuration
            Load();
        }

        public bool Save()
        {
            try
            {
                string configPath = GetConfigPath();

                var configDirectory = Path.GetDirectoryName(configPath);
                if (!Directory.Exists(configDirectory))
                    Directory.CreateDirectory(configDirectory);

                XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                using (FileStream fs = new FileStream(configPath, FileMode.Create, FileAccess.ReadWrite))
                    serializer.Serialize(fs, configuration);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Load()
        {
            try
            {
                string configPath = GetConfigPath();

                if (File.Exists(configPath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                    using (FileStream fs = new FileStream(configPath, FileMode.Open, FileAccess.Read))
                    {
                        Configuration newConfiguration = serializer.Deserialize(fs) as Configuration;

                        // Possible validation

                        configuration = newConfiguration;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        // Public properties --------------------------------------------------

        public Configuration Configuration => configuration;
    }
}
