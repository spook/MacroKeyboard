﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MacroKeyboard.Satellite.Services
{
    public class PathService
    {
        private const string PUBLISHER = "Spooksoft";
        private const string APPNAME = "MacroKeyboardSatellite";
        private const string CONFIG_NAME = "config.xml";

        private readonly string appDataPath;

        public PathService()
        {
            appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), PUBLISHER, APPNAME);
            Directory.CreateDirectory(appDataPath);
        }

        public string AppDataPath => appDataPath;

        public string GetConfigFilePath() => Path.Combine(appDataPath, CONFIG_NAME);
    }
}
