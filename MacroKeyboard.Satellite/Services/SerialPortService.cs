﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RJCP.IO.Ports;

namespace MacroKeyboard.Satellite.Services
{
    public class SerialPortService
    {
        // Private constants --------------------------------------------------

        private const string SerialPortLogSource = "Serial port service";

        // Private fields -----------------------------------------------------

        private SerialPortStream serialPortStream;
        private bool opened;
        private string port;
        private readonly ConfigurationService configurationService;
        private readonly LogService logService;

        // Private methods ----------------------------------------------------

        private void HandleSerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
#if DEBUG
            logService.AddLog(SerialPortLogSource, $"Data received");
#endif
            DataReceived?.Invoke(this, EventArgs.Empty);
        }

        private void HandleSerialPortErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
#if DEBUG
            logService.AddLog(SerialPortLogSource, $"Serial port received error!");
#endif
        }

        // Public methods -----------------------------------------------------

        public SerialPortService(ConfigurationService configurationService, LogService logService)
        {
            this.configurationService = configurationService;
            this.logService = logService;

            serialPortStream = new SerialPortStream();
            serialPortStream.BaudRate = 115200;
            serialPortStream.DataReceived += HandleSerialPortDataReceived;
            serialPortStream.ErrorReceived += HandleSerialPortErrorReceived;
            serialPortStream.ReceivedBytesThreshold = 1;
            serialPortStream.ReadTimeout = 250;

            string[] ports = SerialPortStream.GetPortNames();

            if (ports.Any(p => p == configurationService.Configuration.LastUsedPort))
                OpenPort(configurationService.Configuration.LastUsedPort);
        }

        public void OpenPort(string port)
        {
#if DEBUG
            logService.AddLog(SerialPortLogSource, $"Trying to open port {port}...");
#endif

            ClosePort();

            serialPortStream.PortName = port;

            try
            {
#if DEBUG
                logService.AddLog(SerialPortLogSource, $"Opening port {port}...");
#endif
                serialPortStream.Open();
#if DEBUG
                if (serialPortStream.IsOpen)
                    logService.AddLog(SerialPortLogSource, $"Opened port {port}...");
                else
                    logService.AddLog(SerialPortLogSource, $"Open call exited, but port is not opened");
#endif

                opened = true;
                this.port = port;

                configurationService.Configuration.LastUsedPort = port;
                configurationService.Save();
#if DEBUG
                logService.AddLog(SerialPortLogSource, $"Bytes to read: {serialPortStream.BytesToRead}");
#endif

                DataReceived?.Invoke(this, EventArgs.Empty);
            }
            catch
            {
#if DEBUG
                logService.AddLog(SerialPortLogSource, $"Failed to open port {port}...");
#endif

                opened = false;
                this.port = null;
            }
        }

        public void ClosePort()
        {
#if DEBUG
            logService.AddLog(SerialPortLogSource, $"Closing port {this.port}...");
#endif

            if (serialPortStream.IsOpen)
            {
                serialPortStream.Close();
                opened = false;
                this.port = null;
            }
        }

        public string[] GetAvailablePorts() => SerialPortStream.GetPortNames().Distinct().ToArray();

        public int ReadBytes(byte[] byteBuffer, int count)
        {
#if DEBUG
            logService.AddLog(SerialPortLogSource, $"Attempting to read {count} bytes...");
#endif

            if (!opened)
            {
#if DEBUG
                logService.AddLog(SerialPortLogSource, $"Port is not open, nothing got read.");
#endif
                return -1;
            }

            try
            {
                int result = serialPortStream.Read(byteBuffer, 0, count);
#if DEBUG
                logService.AddLog(SerialPortLogSource, $"Managed to read {result} bytes.");
#endif

                return result;
            }
            catch (Exception e)
            {
#if DEBUG
                logService.AddLog(SerialPortLogSource, $"Failed to read bytes: {e.Message}");
#endif

                ClosePort();
                return -1;
            }
        }

        public (bool result, byte data) ReadByte()
        {
            if (!opened)
                return (false, 0);

            try
            {
                int readData = serialPortStream.ReadByte();
                if (readData < 0)
                {
                    return (false, 0);
                }
                else
                {
                    return (true, (byte)readData);
                }
            }
            catch (TimeoutException)
            {
                return (false, 0);
            }
            catch
            {
                ClosePort();
                return (false, 0);
            }
        }

        // Public properties --------------------------------------------------

        public string Port => port;

        public event EventHandler DataReceived;
    }
}
