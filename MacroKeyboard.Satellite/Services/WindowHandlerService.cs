﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Services
{
    public class WindowHandlerService : IDisposable
    {
        // Private fields -----------------------------------------------------

        private IntPtr instance;

        // Private methods ----------------------------------------------------

        [DllImport("MacroKeyboard.Satellite.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void Initialize();

        [DllImport("MacroKeyboard.Satellite.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void Deinitialize();

        [DllImport("MacroKeyboard.Satellite.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr CreateWindowHandler();

        [DllImport("MacroKeyboard.Satellite.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void DestroyWindowHandler(IntPtr handler);

        [DllImport("MacroKeyboard.Satellite.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern bool ActivateMainWindow(IntPtr handler, [MarshalAs(UnmanagedType.LPWStr)] string processName);

        // Public methods -----------------------------------------------------

        public WindowHandlerService()
        {
            Initialize();
            instance = CreateWindowHandler();
        }

        public void ActivateMainWindow(string processName)
        {
            ActivateMainWindow(instance, processName);
        }

        public void Dispose()
        {
            DestroyWindowHandler(instance);
            instance = IntPtr.Zero;

            Deinitialize();
        }
    }
}
