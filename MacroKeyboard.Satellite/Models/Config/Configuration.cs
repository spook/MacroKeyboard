﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroKeyboard.Satellite.Models.Config
{
    [XmlRoot("Configuration")]
    public class Configuration
    {
        [XmlElement("LastUsedPort")]
        public string? LastUsedPort { get; set; } = null;
    }
}
