﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Models.Commands
{
    public class ActivateCommand : BaseCommand
    {
        public ActivateCommand(string binary)
        {
            Binary = binary;
        }

        public string Binary { get; }
    }
}
