﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Models.Commands
{
    public class ScreenCommand : BaseCommand
    {
        public ScreenCommand(Bitmap screen)
        {
            Screen = screen;
        }

        public Bitmap Screen { get; }
    }
}
