﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Models.Commands
{
    public class ExecuteCommand : BaseCommand
    {
        public ExecuteCommand(string command)
        {
            Command = command;
        }

        public string Command { get; }
    }
}
