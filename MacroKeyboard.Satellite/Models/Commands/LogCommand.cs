﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroKeyboard.Satellite.Models.Commands
{
    public class LogCommand : BaseCommand
    {
        public LogCommand(string log)
        {
            Log = log;
        }

        public string Log { get; }
    }
}
